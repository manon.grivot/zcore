package me.zodiakk.zcore.modules;

public class UnknownModuleException extends NullPointerException {
    private static final long serialVersionUID = -2841267453453349319L;

    public UnknownModuleException(String name) {
        super("The requested module (" + name + ") does not exists in the current context.");
    }

    public UnknownModuleException(Class<?> clazz) {
        super("The requested module (" + clazz.toString() + ") does not exists in the current context.");
    }
}
