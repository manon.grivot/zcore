package me.zodiakk.zcore.modules;

/**
 * A simple container class to store a module and its informations.
 * @param <T> Module class
 */
public final class ModuleContainer<T extends AbstractModule> {
    private final Class<T> clazz;
    private final T module;
    private final String name;
    private final Integer order;

    // Keep trace of the enabling order
    private static Integer lastOrder = 0;

    /**
     * Create a module container.
     * @param clazz Module class object
     * @param module Module
     * @param name Module name
     */
    public ModuleContainer(Class<T> clazz, T module, String name) {
        this.clazz = clazz;
        this.module = module;
        this.name = name;
        this.order = lastOrder++;
    }

    /**
     * Get the module class.
     * @return Module class
     */
    public Class<T> getModuleClass() {
        return clazz;
    }

    /**
     * Get the actual module.
     * @return Module
     */
    public T getModule() {
        return module;
    }

    /**
     * Get the module name.
     * @return Module name
     */
    public String getName() {
        return name;
    }

    /**
     * Get the enabling order of this module.
     * @return Enabling order
     */
    public Integer getOrder() {
        return order;
    }
}
