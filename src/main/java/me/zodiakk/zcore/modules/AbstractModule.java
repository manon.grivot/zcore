package me.zodiakk.zcore.modules;

import me.zodiakk.zapi.config.JsonConfigurationSection;
import me.zodiakk.zcore.ZCore;
import me.zodiakk.zcore.ZCoreConfig;
import me.zodiakk.zcore.ZCoreLogger;

public abstract class AbstractModule {
    private final String moduleName;
    private ModuleStatus moduleStatus;
    private Exception lastError;

    public AbstractModule(String moduleName) {
        this.moduleName = moduleName;
        this.moduleStatus = ModuleStatus.DISABLED;
    }

    public final String getName() {
        return moduleName;
    }

    public final ModuleStatus getStatus() {
        return moduleStatus;
    }

    public final ModuleStatus setStatus(ModuleStatus newStatus) {
        moduleStatus = newStatus;
        return moduleStatus;
    }

    public final Exception getError() {
        return lastError;
    }

    public final void setError(Exception exception) {
        setStatus(ModuleStatus.CRASHED);
        lastError = exception;
        ZCoreLogger.error(moduleName, exception);
    }

    public final ModuleStatus forceReload(ZCore core) {
        ModuleStatus unloadStatus = disable(core);

        if (unloadStatus != ModuleStatus.DISABLED) {
            return unloadStatus;
        }
        return enable(core);
    }

    public final JsonConfigurationSection getConfig() {
        return ZCoreConfig.getInstance().getModuleConfig(moduleName);
    }

    public abstract ModuleStatus enable(ZCore core);

    public abstract ModuleStatus disable(ZCore core);

    public abstract ModuleStatus reload(ZCore core);
}
