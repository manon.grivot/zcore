package me.zodiakk.zcore.modules;

public enum ModuleStatus {
    /**
     * The module is enabled and running.
     */
    ENABLED("§aEnabled"),

    /**
     * The module has been disabled or were never enabled.
     */
    DISABLED("§7Disabled"),

    /**
     * The module crashed while running and couldn't recover.
     */
    CRASHED("§cCrashed"),

    /**
     * The module failed to load and is disabled.
     */
    FAILED_TO_LOAD("§cFailed to load"),

    /**
     * The module has been disabled but errors occurred while disabling it.
     */
    DISABLED_ERROR("§7Disabled with errors"),

    /**
     * Special status for modules that cannot reload.
     * <p>Note: This status will never be returned from a call to {@link AbstractModule#getStatus()}
     */
    NO_RELOAD("§eCouldn't reload"),

    /**
     * The module has been disabled in the configuration file.
     */
    DISABLED_CONFIG("§7Disabled in configuration"),

    /**
     * Unknown status.
     */
    OTHER("§7Unknown");

    private final String message;

    private ModuleStatus(String message) {
        this.message = message;
    }

    public final String getMessage() {
        return message;
    }
}
