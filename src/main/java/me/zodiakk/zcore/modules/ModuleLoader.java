package me.zodiakk.zcore.modules;

import java.util.HashSet;

import me.zodiakk.zcore.ZCore;
import me.zodiakk.zcore.ZCoreConfig;
import me.zodiakk.zcore.ZCoreLogger;

/**
 * This class provides various methods to load, enable, disable, reload and get modules.
 *
 * @author Zodiak
 * @since 0.1
 */
public class ModuleLoader {
    private HashSet<ModuleContainer<?>> modules;
    private ZCore core;

    /**
     * Internal constructor.
     * @param core ZCore instance
     */
    public ModuleLoader(ZCore core) {
        this.core = core;
        this.modules = new HashSet<ModuleContainer<?>>();
    }

    /**
     * Load a new module, and enable it if needed.
     * @param <T> Module type
     * @param module Module instance
     * @param clazz Module class type
     * @param name Module name
     */
    public <T extends AbstractModule> void loadModule(Class<T> clazz, T module, String name) {
        loadModule(new ModuleContainer<T>(clazz, module, name));
    }

    /**
     * Load a new module, and enable it if needed.
     * @param <T> Module type
     * @param container Module container
     */
    public <T extends AbstractModule> void loadModule(ModuleContainer<T> container) {
        modules.add(container);

        if (!ZCoreConfig.getInstance().isModuleDisabled(container.getName())) {
            enableModule(container);
        } else {
            container.getModule().setStatus(ModuleStatus.DISABLED_CONFIG);
        }
    }

    /**
     * Enable a module based on its name.
     * @param name Module name
     * @return Status of the module after being enabled
     * @throws UnknownModuleException If the module doesn't exist
     */
    public ModuleStatus enableModule(String name) throws UnknownModuleException {
        ModuleContainer<?> container = getModuleContainer(name);

        if (container.getModule().getStatus().equals(ModuleStatus.ENABLED)) {
            return container.getModule().getStatus();
        }
        return enableModule(container);
    }

    /**
     * Enable a module based on its class.
     * @param clazz Module class
     * @return Status of the module after being enabled
     * @throws UnknownModuleException If the module doesn't exist
     */
    public ModuleStatus enableModule(Class<? extends AbstractModule> clazz) throws UnknownModuleException {
        ModuleContainer<?> container = getModuleContainer(clazz);

        return enableModule(container);
    }

    /**
     * Enable a module.
     * @param container Module container
     * @return Status of the module after being enabled
     */
    public ModuleStatus enableModule(ModuleContainer<?> container) {
        if (container.getModule().getStatus().equals(ModuleStatus.ENABLED)) {
            ZCoreLogger.warn("Tried to manually enable " + container.getName() + " module, but it was already running.");
            return container.getModule().getStatus();
        }
        if (container.getModule().getStatus().equals(ModuleStatus.DISABLED_CONFIG)) {
            ZCoreLogger.warn("Tried to manually enable " + container.getName() + " module, but it has been disabled in the configuration file.");
            return ModuleStatus.DISABLED_CONFIG;
        }
        ZCoreLogger.log("Enabling " + container.getName() + " module...");

        ModuleStatus newStatus;
        try {
            newStatus = container.getModule().enable(core);
        } catch (Exception ex) {
            container.getModule().setError(ex);
            newStatus = container.getModule().setStatus(ModuleStatus.FAILED_TO_LOAD);
        }
        if (newStatus.equals(ModuleStatus.ENABLED)) {
            ZCoreLogger.log("Enabled " + container.getName() + " module.");
        } else {
            ZCoreLogger.error("Failed to enable " + container.getName() + " module.");
        }
        return newStatus;
    }

    /**
     * Disable a module based on its name.
     * @param name Module name
     * @return Status of the module after being disabled
     * @throws UnknownModuleException If the module doesn't exist
     */
    public ModuleStatus disableModule(String name) throws UnknownModuleException {
        ModuleContainer<?> container = getModuleContainer(name);

        return disableModule(container);
    }

    /**
     * Disable a module based on its class.
     * @param clazz Module class
     * @return Status of the module after being disabled
     * @throws UnknownModuleException If the module doesn't exist
     */
    public ModuleStatus disableModule(Class<? extends AbstractModule> clazz) throws UnknownModuleException {
        ModuleContainer<?> container = getModuleContainer(clazz);

        return disableModule(container);
    }

    /**
     * Disable a module.
     * @param container Module container
     * @return Status of the module after being disabled
     */
    public ModuleStatus disableModule(ModuleContainer<?> container) {
        if (!container.getModule().getStatus().equals(ModuleStatus.ENABLED)) {
            ZCoreLogger.warn("Tried to manually disable " + container.getName() + " module, but it wasn't running.");
            return container.getModule().getStatus();
        }
        ZCoreLogger.log("Disabling " + container.getName() + " module...");

        ModuleStatus newStatus;
        try {
            newStatus = container.getModule().disable(core);
        } catch (Exception ex) {
            container.getModule().setError(ex);
            newStatus = container.getModule().setStatus(ModuleStatus.DISABLED_ERROR);
        }
        if (newStatus.equals(ModuleStatus.DISABLED)) {
            ZCoreLogger.log("Disabled " + container.getName() + " module.");
        } else {
            ZCoreLogger.error("An error occured while disabling " + container.getName() + " module: " + container.getModule().getError().getMessage() + ".");
        }
        return newStatus;
    }

    /**
     * Reload a module based on its name.
     * @param name Module name
     * @return Status of the module after being reloaded
     * @throws UnknownModuleException If the module doesn't exist
     */
    public ModuleStatus reloadModule(String name) throws UnknownModuleException {
        ModuleContainer<?> container = getModuleContainer(name);

        return reloadModule(container);
    }

    /**
     * Reload a module based on its class.
     * @param clazz Module class
     * @return Status of the module after being reloaded
     * @throws UnknownModuleException If the module doesn't exist
     */
    public ModuleStatus reloadModule(Class<? extends AbstractModule> clazz) throws UnknownModuleException {
        ModuleContainer<?> container = getModuleContainer(clazz);

        return reloadModule(container);
    }

    /**
     * Reload a module.
     * @param container Module container
     * @return Status of the module after being reloaded
     */
    public ModuleStatus reloadModule(ModuleContainer<?> container) {
        if (container.getModule().getStatus().equals(ModuleStatus.DISABLED_CONFIG)) {
            ZCoreLogger.warn("Tried to manually reload " + container.getName() + " module, but it has been disabled in the configuration file.");
            return container.getModule().getStatus();
        }
        if (!container.getModule().getStatus().equals(ModuleStatus.ENABLED)) {
            ZCoreLogger.warn("Tried to manually reload " + container.getName() + " module, but it is not enabled.");
            return container.getModule().getStatus();
        }

        ModuleStatus newStatus = container.getModule().reload(core);
        if (newStatus.equals(ModuleStatus.ENABLED)) {
            ZCoreLogger.log("Reloaded " + container.getName() + " module.");
        } else {
            ZCoreLogger.error("Failed to reload " + container.getName() + " module.");
        }
        return newStatus;
    }

    /**
     * Try to force-enable a module based on its name.
     * <p>Note that this method will ignore configuration file, which could lead to undefined behavior
     * @param name Module name
     * @return Status of the module after being force-enabled
     * @throws UnknownModuleException If the module doesn't exist
     */
    public ModuleStatus forceEnableModule(String name) throws UnknownModuleException {
        ModuleContainer<?> container = getModuleContainer(name);

        return forceEnableModule(container);
    }

    /**
     * Try to force-enable a module based on its class.
     * <p>Note that this method will ignore configuration file, which could lead to undefined behavior
     * @param clazz Module class
     * @return Status of the module after being force-enabled
     * @throws UnknownModuleException If the module doesn't exist
     */
    public ModuleStatus forceEnableModule(Class<? extends AbstractModule> clazz) throws UnknownModuleException {
        ModuleContainer<?> container = getModuleContainer(clazz);

        return forceEnableModule(container);
    }

    /**
     * Try to force-enable a module.
     * <p>Note that this method will ignore configuration file, which could lead to undefined behavior
     * @param container Module container
     * @return Status of the module after being force-enabled
     */
    public ModuleStatus forceEnableModule(ModuleContainer<?> container) {
        ZCoreLogger.warn("Trying to force-enable " + container.getName() + " module...");
        ZCoreLogger.log("Enabling " + container.getName() + " module...");

        ModuleStatus newStatus;
        try {
            newStatus = container.getModule().enable(core);
        } catch (Exception ex) {
            container.getModule().setError(ex);
            newStatus = container.getModule().setStatus(ModuleStatus.FAILED_TO_LOAD);
        }
        if (newStatus.equals(ModuleStatus.ENABLED)) {
            ZCoreLogger.log("Enabled " + container.getName() + " module.");
        } else {
            ZCoreLogger.error("Failed to enable " + container.getName() + " module.");
        }
        return newStatus;
    }

    /**
     * Try to force-reload a module based on its name.
     * <p>Force-reloading will not call {@link AbstractModule#reload(ZCore)}, and will instead successively call
     * {@link AbstractModule#disable(ZCore)} and {@link AbstractModule#enable(ZCore)}
     * @param name Module name
     * @return Status of the module after being force-reloaded
     * @throws UnknownModuleException If the module doesn't exist
     */
    public ModuleStatus forceReloadModule(String name) throws UnknownModuleException {
        ModuleContainer<?> container = getModuleContainer(name);

        return forceReloadModule(container);
    }

    /**
     * Try to force-reload a module based on its class.
     * <p>Force-reloading will not call {@link AbstractModule#reload(ZCore)}, and will instead successively call
     * {@link AbstractModule#disable(ZCore)} and {@link AbstractModule#enable(ZCore)}
     * @param clazz Module class
     * @return Status of the module after being force-reloaded
     * @throws UnknownModuleException If the module doesn't exist
     */
    public ModuleStatus forceReloadModule(Class<? extends AbstractModule> clazz) throws UnknownModuleException {
        ModuleContainer<?> container = getModuleContainer(clazz);

        return forceReloadModule(container);
    }

    public ModuleStatus forceReloadModule(ModuleContainer<?> container) {
        ZCoreLogger.warn("Force-reloading " + container.getName() + " module...");

        ModuleStatus newStatus = container.getModule().forceReload(core);
        if (newStatus == ModuleStatus.ENABLED) {
            ZCoreLogger.log("Reloaded " + container.getName() + " module.");
        } else {
            ZCoreLogger.error("Failed to reload " + container.getName() + " module.");
        }
        return newStatus;
    }

    /**
     * Get the status of a module based on its name.
     * @param name Module name
     * @return Module status
     * @throws UnknownModuleException If the module doesn't exist
     */
    public ModuleStatus getModuleStatus(String name) throws UnknownModuleException {
        return getModule(name).getStatus();
    }

    /**
     * Get the status of a module based on its class.
     * @param clazz Module class
     * @return Module status
     * @throws UnknownModuleException If the module doesn't exist
     */
    public ModuleStatus getModuleStatus(Class<? extends AbstractModule> clazz) throws UnknownModuleException {
        return getModule(clazz).getStatus();
    }

    /**
     * Get the status of a module.
     * @param container Module container
     * @return Module status
     */
    public ModuleStatus getModuleStatus(ModuleContainer<? extends AbstractModule> container) {
        return container.getModule().getStatus();
    }

    /**
     * Get the last error message of a module based on its name.
     * <p>If there is no error, this method will return {@code null}
     * @param name Module name
     * @return Module last error
     * @throws UnknownModuleException If the module doesn't exist
     */
    public String getModuleLastError(String name) throws UnknownModuleException {
        ModuleContainer<?> container = getModuleContainer(name);

        return getModuleLastError(container);
    }

    /**
     * Get the last error message of a module based on its class.
     * <p>If there is no error, this method will return {@code null}
     * @param clazz Module class
     * @return Module last error
     * @throws UnknownModuleException If the module doesn't exist
     */
    public String getModuleLastError(Class<? extends AbstractModule> clazz) throws UnknownModuleException {
        ModuleContainer<?> container = getModuleContainer(clazz);

        return getModuleLastError(container);
    }

    /**
     * Get the last error message of a module.
     * <p>If there is no error, this method will return {@code null}
     * @param container Module container
     * @return Module last error
     */
    public String getModuleLastError(ModuleContainer<?> container) {
        if (container.getModule().getError() != null) {
            return container.getModule().getError().getMessage();
        }
        return null;
    }

    /**
     * Check whether a module exists or not.
     * @param name Module name
     * @return True if the module exists, false elsewise
     */
    public boolean hasModule(String name) {
        for (ModuleContainer<?> module : modules) {
            if (module.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check whether a module exists or not.
     * @param clazz Module class
     * @return True if the module exists, false elsewise
     */
    public boolean hasModule(Class<? extends AbstractModule> clazz) {
        for (ModuleContainer<?> module : modules) {
            if (module.getClass().isAssignableFrom(clazz)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get a module based on its name.
     * @param name Module name
     * @return The actual module
     * @throws UnknownModuleException If the module doesn't exist
     */
    public AbstractModule getModule(String name) throws UnknownModuleException {
        for (ModuleContainer<?> module : modules) {
            if (module.getName().equals(name)) {
                return module.getModule();
            }
        }
        throw new UnknownModuleException(name);
    }

    /**
     * Get a module based on its class.
     * @param <T> Module type
     * @param clazz Module class
     * @return The actual module
     */
    public <T extends AbstractModule> T getModule(Class<T> clazz) {
        for (ModuleContainer<?> module : modules) {
            if (clazz.isInstance(module.getModule())) {
                return clazz.cast(module.getModule());
            }
        }
        throw new UnknownModuleException(clazz);
    }

    /**
     * Get all modules.
     * @return All modules
     */
    public HashSet<AbstractModule> getAllModules() {
        HashSet<AbstractModule> res = new HashSet<AbstractModule>();

        for (ModuleContainer<?> container : modules) {
            res.add(container.getModule());
        }
        return res;
    }

    private ModuleContainer<?> getModuleContainer(String name) throws UnknownModuleException {
        for (ModuleContainer<?> container : modules) {
            if (container.getName().equals(name)) {
                return container;
            }
        }
        throw new UnknownModuleException(name);
    }

    private <T extends AbstractModule> ModuleContainer<?> getModuleContainer(Class<T> clazz) throws UnknownModuleException {
        for (ModuleContainer<?> container : modules) {
            if (clazz.isInstance(container.getModule())) {
                return container;
            }
        }
        throw new UnknownModuleException(clazz);
    }

    /**
     * Get all module containers.
     * @return All module containers
     */
    public HashSet<ModuleContainer<?>> getAllModuleContainers() {
        return modules;
    }
}
