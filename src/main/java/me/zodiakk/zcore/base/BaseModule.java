package me.zodiakk.zcore.base;

import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.data.table.TableField;
import me.zodiakk.zapi.data.table.TableField.DefaultValue;
import me.zodiakk.zapi.data.table.TableField.FieldType;
import me.zodiakk.zapi.data.table.TableField.IndexType;
import me.zodiakk.zapi.data.table.TableModel;
import me.zodiakk.zcore.ZCore;
import me.zodiakk.zcore.modules.AbstractModule;
import me.zodiakk.zcore.modules.ModuleStatus;

public class BaseModule extends AbstractModule {
    PlayerDataListener playerDataListener;
    BasePlaceholder basePlaceholder;

    public BaseModule() {
        super("Base");
    }

    @Override
    public ModuleStatus enable(ZCore core) {
        try {
            TableModel players = ZApi.getData().getTableManager().getModel("players");

            if (players == null) {
                players = new TableModel("players");
                ZApi.getData().getTableManager().setModel(players);
            }

            players.addField("uuid", new TableField<String>("uuid", FieldType.VARCHAR, 36, IndexType.PRIMARY));
            players.addField("last_name", new TableField<String>("last_name", FieldType.VARCHAR, 32, new DefaultValue<String>("unknown")));
            players.addField("first_join", new TableField<Date>("first_join", FieldType.DATETIME));
            players.addField("last_join", new TableField<Date>("last_join", FieldType.DATETIME));
            players.addField("connections", new TableField<Integer>("connections", FieldType.INT, new DefaultValue<Integer>(0)));
        } catch (Exception ex) {
            ZApi.postError(ex);
            return setStatus(ModuleStatus.FAILED_TO_LOAD);
        }
        playerDataListener = new PlayerDataListener();
        Bukkit.getServer().getPluginManager().registerEvents(playerDataListener, core);
        basePlaceholder = new BasePlaceholder(core);
        basePlaceholder.register();
        return setStatus(ModuleStatus.ENABLED);
    }

    @Override
    public ModuleStatus disable(ZCore core) {
        HandlerList.unregisterAll(playerDataListener);
        return setStatus(ModuleStatus.DISABLED);
    }

    @Override
    public ModuleStatus reload(ZCore core) {
        if (disable(core) != ModuleStatus.DISABLED) {
            return getStatus();
        }
        return enable(core);
    }
}
