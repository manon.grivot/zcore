package me.zodiakk.zcore.base;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.UUID;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.data.filter.WhereFilter;
import me.zodiakk.zapi.data.filter.WhereOperator;
import me.zodiakk.zapi.data.filter.WhereOperatorType;
import me.zodiakk.zapi.data.result.Result;
import me.zodiakk.zapi.data.table.TableModel;
import me.zodiakk.zapi.data.table.TableRow;

public class PlayerDataListener implements Listener {
    TableModel players;

    public PlayerDataListener() {
        players = ZApi.getData().getTableManager().getModel("players");
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        ZApi.getThreadsManager().execute(new Runnable() {
            @Override
            public void run() {
                UUID uuid = event.getPlayer().getUniqueId();
                TableRow player = getPlayerRow(uuid);

                if (player == null) {
                    ZApi.getThreadsManager().executeInMainThread(new Runnable() {
                        @Override
                        public void run() {
                            event.getPlayer().kickPlayer("§cUne erreur est survenue lors de votre connexion. \n§cSi le problème persiste, merci de contacter un administrateur.");
                        }
                    });
                    return;
                }

                player.setValue("last_join", Calendar.getInstance().getTime());
                player.setValue("last_name", event.getPlayer().getName());
                player.setValue("connections", ((Integer) (player.getValue("connections").getValue())) + 1);
                try {
                    players.updateRows(player);
                } catch (SQLException ex) {
                    ZApi.postError(ex, uuid);
                    ZApi.getThreadsManager().executeInMainThread(new Runnable() {
                        @Override
                        public void run() {
                            event.getPlayer().kickPlayer("§cUne erreur est survenue lors de votre connexion. \n§cSi le problème persiste, merci de contacter un administrateur.");
                        }
                    });
                    return;
                }
            }
        });
    }

    private TableRow getPlayerRow(UUID uuid) {
        try {
            WhereFilter queryWhere = new WhereFilter();
            Result select;

            queryWhere.addOperator(new WhereOperator("uuid", WhereOperatorType.EQ, uuid.toString()));
            select = players.select(queryWhere);
            if (select.getResults().size() == 0) {
                TableRow player = new TableRow(players);

                player.setValue("first_join", Calendar.getInstance().getTime());
                player.setValue("last_join", Calendar.getInstance().getTime());
                player.setValue("uuid", uuid.toString());
                players.insert(player);
                return getPlayerRow(uuid);
            }
            return select.getResults().get(0).toTableRow();
        } catch (SQLException ex) {
            ZApi.postError(ex, uuid);
            return null;
        }
    }
}
