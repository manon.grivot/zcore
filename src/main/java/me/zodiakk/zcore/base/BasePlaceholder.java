package me.zodiakk.zcore.base;

import java.sql.SQLException;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.data.filter.WhereFilter;
import me.zodiakk.zapi.data.filter.WhereOperator;
import me.zodiakk.zapi.data.filter.WhereOperatorType;
import me.zodiakk.zapi.data.result.Result;

public class BasePlaceholder extends PlaceholderExpansion {
    private Plugin plugin;

    public BasePlaceholder(Plugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getAuthor() {
        return "Zodiak";
    }

    @Override
    public String getIdentifier() {
        return "zcore-base";
    }

    @Override
    public String getVersion() {
        return plugin.getDescription().getVersion();
    }

    @Override
    public String onPlaceholderRequest(Player player, String identifier) {
        if (player == null) {
            return "";
        }

        if (identifier.equals("connections")) {
            WhereFilter where = new WhereFilter();
            WhereOperator whereOperator = new WhereOperator("uuid", WhereOperatorType.EQ,
                    player.getUniqueId().toString());
            where.addOperator(whereOperator);
            try {
                Result res = ZApi.getData().getTableManager().getModel("players").select(where);
                return res.getResults().get(0).getValue("connections").getValue().toString();
            } catch (SQLException ex) {
                ZApi.postError(ex, player);
                return "Erreur";
            }
        }

        return "";
    }
}
