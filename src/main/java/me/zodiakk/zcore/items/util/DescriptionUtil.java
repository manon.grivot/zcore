package me.zodiakk.zcore.items.util;

import java.util.TreeMap;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.potion.PotionEffectType;

import me.zodiakk.zapi.util.Attributes.Operation;

public class DescriptionUtil {
    private static final TreeMap<Integer, String> ROMAN_NUMBER_MAP = new TreeMap<Integer, String>();

    static {
        ROMAN_NUMBER_MAP.put(1000, "M");
        ROMAN_NUMBER_MAP.put(900, "CM");
        ROMAN_NUMBER_MAP.put(500, "D");
        ROMAN_NUMBER_MAP.put(400, "CD");
        ROMAN_NUMBER_MAP.put(100, "C");
        ROMAN_NUMBER_MAP.put(90, "XC");
        ROMAN_NUMBER_MAP.put(50, "L");
        ROMAN_NUMBER_MAP.put(40, "XL");
        ROMAN_NUMBER_MAP.put(10, "X");
        ROMAN_NUMBER_MAP.put(9, "IX");
        ROMAN_NUMBER_MAP.put(5, "V");
        ROMAN_NUMBER_MAP.put(4, "IV");
        ROMAN_NUMBER_MAP.put(1, "I");
    }

    public static Double getAttackDamage(Material material) {
        switch (material) {
        case DIAMOND_SWORD:
            return 8.0d;
        case DIAMOND_AXE: case IRON_SWORD:
            return 7.0d;
        case DIAMOND_PICKAXE: case IRON_AXE: case STONE_SWORD:
            return 6.0d;
        case DIAMOND_SPADE: case IRON_PICKAXE: case STONE_AXE: case WOOD_SWORD: case GOLD_SWORD:
            return 5.0d;
        case IRON_SPADE: case STONE_PICKAXE: case WOOD_AXE: case GOLD_AXE:
            return 4.0d;
        case STONE_SPADE: case WOOD_PICKAXE: case GOLD_PICKAXE:
            return 3.0d;
        case WOOD_SPADE: case GOLD_SPADE:
            return 2.0d;
        default:
            return 1.0d;
        }
    }

    public static String getAttributesText(Operation operation, double amount) {
        if (operation.equals(Operation.ADD_NUMBER)) {
            return "+" + amount;
        } else if (operation.equals(Operation.ADD_PERCENTAGE)) {
            return "+" + amount + "%";
        } else {
            return "x" + (Double.valueOf(amount) / 100);
        }
    }

    public static String getLevelNice(Integer level) {
        if (level < 1) {
            return "";
        } else if (level > 25) {
            return "" + level;
        }

        int key = ROMAN_NUMBER_MAP.floorKey(level);

        if (level == key) {
            return ROMAN_NUMBER_MAP.get(level);
        }

        return ROMAN_NUMBER_MAP.get(key) + getLevelNice(level - key);
    }

    public static String getEnchantmentDescription(Enchantment ench, Integer level) {
        switch (ench.getName().toLowerCase()) {
        case "water_worker":
            return "&7Affinité aquatique";
        case "damage_arthropods":
            return "&7Fléau des arthropodes &c" + getLevelNice(level);
        case "protection_explosions":
            return "&7Protection (explosions) &c" + getLevelNice(level);
        case "depth_strider":
            return "&7Agilité aquatique &c" + getLevelNice(level);
        case "dig_speed":
            return "&7Efficacité &c" + getLevelNice(level);
        case "protection_fall":
            return "&7Protection (chutes) &c" + getLevelNice(level);
        case "fire_aspect":
            return "&7Aura de feu &c" + getLevelNice(level);
        case "protection_fire":
            return "&7Protection (feu) &c" + getLevelNice(level);
        case "arrow_fire":
            return "&7Flèches enflammées";
        case "loot_bonus_blocks":
            return "&7Fortune &c" + getLevelNice(level);
        case "arrow_infinite":
            return "&7Flèches infinies";
        case "knockback":
            return "&7Recul &c" + getLevelNice(level);
        case "loot_bonus_mobs":
            return "&7Butin &c" + getLevelNice(level);
        case "luck":
            return "&7Butin (pêche) &c" + getLevelNice(level);
        case "lure":
            return "&7Appât amélioré &c" + getLevelNice(level);
        case "arrow_damage":
            return "&7Puissance &c" + getLevelNice(level);
        case "protection_projectile":
            return "&7Protection (projectiles) &c" + getLevelNice(level);
        case "protection_environmental":
            return "&7Protection &c" + getLevelNice(level);
        case "arrow_knockback":
            return "&7Recul (flèches) &c" + getLevelNice(level);
        case "oxygen":
            return "&7Respiration &c" + getLevelNice(level);
        case "damage_all":
            return "&7Tranchant &c" + getLevelNice(level);
        case "silk_touch":
            return "&7Toucher de soie";
        case "damage_undead":
            return "&7Châtiment &c" + getLevelNice(level);
        case "thorns":
            return "&7Épines &c" + getLevelNice(level);
        case "durability":
            return "&7Durabilité &c" + getLevelNice(level);
        default:
            return "&7error-" + ench.getName().toLowerCase() + " &c" + getLevelNice(level);
        }
    }

    public static String getPotionEffect(PotionEffectType name, Integer level, Integer time) {
        String potion = "";
        switch (name.getName().toLowerCase()) {
        case "speed":
            potion += "Rapidité";
            break;
        case "slow":
            potion += "Lenteur";
            break;
        case "fast_digging":
            potion += "Célérité";
            break;
        case "slow_digging":
            potion += "Fatigue de minage";
            break;
        case "increase_damage":
            potion += "Force";
            break;
        case "heal":
            potion += "Soin instantané";
            break;
        case "harm":
            potion += "Dégats instantanés";
            break;
        case "jump":
            potion += "Sauts améliorés";
            break;
        case "confusion":
            potion += "Nausée";
            break;
        case "regeneration":
            potion += "Régénération";
            break;
        case "damage_resistance":
            potion += "Résistance";
            break;
        case "fire_resistance":
            potion += "Résistance au feu";
            break;
        case "water_breathing":
            potion += "Apnée";
            break;
        case "invisibility":
            potion += "Invisibilité";
            break;
        case "blindness":
            potion += "Aveuglement";
            break;
        case "night_vision":
            potion += "Nyctalopie";
            break;
        case "hunger":
            potion += "Faim";
            break;
        case "weakness":
            potion += "Faiblesse";
            break;
        case "poison":
            potion += "Poison";
            break;
        case "wither":
            potion += "Wither";
            break;
        case "health_boost":
            potion += "Boost de vie";
            break;
        case "absorption":
            potion += "Absorption";
            break;
        case "saturation":
            potion += "Satiété";
            break;
        default:
            potion += "error-" + name.getName().toLowerCase();
            break;
        }
        if (level > 0) {
            potion += " " + getLevelNice(level.intValue());
        }
        if (time != null) {
            potion += " (" + time.intValue() + "s)";
        }
        return potion;
    }

    public static String getPercentage(Double percentage) {
        return "" + Double.valueOf(Double.valueOf(percentage * 10000.0d).intValue() / 100.0d);
    }

    public static String getChance(Double percentage) {
        if (percentage >= 100.0d) {
            return "";
        } else {
            return " &3(Chance : &6" + getPercentage(percentage) + "%&3)";
        }
    }
}
