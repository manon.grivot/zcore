package me.zodiakk.zcore.items;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;

import me.zodiakk.zapi.config.JsonConfigurationSection;
import me.zodiakk.zcore.ZCore;
import me.zodiakk.zcore.ZCoreConfig;
import me.zodiakk.zcore.items.config.CustomItemsConfig;
import me.zodiakk.zcore.items.effects.EffectListener;
import me.zodiakk.zcore.modules.AbstractModule;
import me.zodiakk.zcore.modules.ModuleStatus;

public class CustomItemsModule extends AbstractModule {
    private static CustomItemsModule me;

    CustomItemsConfig config;
    CustomItemsCommand command;
    CustomItemsListener generalListener;
    // // EffectTask effectsTask;
    EffectListener effectsListener;

    // // BukkitTask effectsBtask;

    public CustomItemsModule() {
        super("CustomItems");
        me = this;
    }

    @Override
    public ModuleStatus enable(ZCore core) {
        JsonConfigurationSection jsonConfig = ZCoreConfig.getInstance().getModuleConfig("CustomItems");

        config = new CustomItemsConfig(core, new File(core.getDataFolder(), jsonConfig.getString("customItemsFolder")));
        command = new CustomItemsCommand();
        generalListener = new CustomItemsListener();
        // // effectsTask = new EffectTask();
        effectsListener = new EffectListener();

        core.getCommand("citems").setExecutor(command);
        // // effectsBtask = Bukkit.getServer().getScheduler().runTaskTimer(core, effectsTask, 4L, 4L);
        Bukkit.getServer().getPluginManager().registerEvents(effectsListener, core);
        Bukkit.getServer().getPluginManager().registerEvents(generalListener, core);
        return setStatus(ModuleStatus.ENABLED);
    }

    @Override
    public ModuleStatus disable(ZCore core) {
        HandlerList.unregisterAll(effectsListener);
        HandlerList.unregisterAll(generalListener);
        // // effectsBtask.cancel();

        return setStatus(ModuleStatus.DISABLED);
    }

    @Override
    public ModuleStatus reload(ZCore core) {
        if (disable(core) != ModuleStatus.DISABLED) {
            return getStatus();
        }
        return enable(core);
    }

    public static CustomItemsModule getInstance() {
        return me;
    }
}
