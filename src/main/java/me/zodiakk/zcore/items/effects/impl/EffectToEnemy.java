package me.zodiakk.zcore.items.effects.impl;

import com.google.gson.JsonObject;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.util.RandomUtil;
import me.zodiakk.zcore.items.effects.EffectType;
import me.zodiakk.zcore.items.effects.EventEffect;
import me.zodiakk.zcore.items.util.DescriptionUtil;

public class EffectToEnemy implements EventEffect {
    PotionEffectType id;
    Integer duration;
    Integer level;
    Double chance;
    String description;

    public EffectToEnemy(JsonObject json) {
        this.id = PotionEffectType.getByName(json.get("id").getAsString());
        this.duration = json.get("duration").getAsInt();
        this.level = json.has("level") ? json.get("level").getAsInt() : 1;
        this.chance = json.has("chance") ? json.get("chance").getAsDouble() : 1.0d;
        this.description = new String("&3Effet à l'ennemi lors d'une attaque : &6"
                + DescriptionUtil.getPotionEffect(id, level, duration) + DescriptionUtil.getChance(chance));

        if (level <= 0) {
            level = 1;
        }
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.EFFECT_TO_ENEMY;
    }

    @Override
    public boolean applyEffect(Event _event, ItemStack _item) {
        EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) _event;

        if (!(event.getEntity() instanceof LivingEntity)
                || !(event.getDamager() instanceof LivingEntity)
                || !RandomUtil.testChance(chance)) {
            return false;
        }

        LivingEntity enemy = (LivingEntity) event.getEntity();
        if (enemy.hasPotionEffect(id)) {
            enemy.removePotionEffect(id);
        }
        enemy.addPotionEffect(new PotionEffect(id, duration.intValue() * 20, this.level.intValue() - 1), true);

        ZApi.logDebug("Applied effect to enemy effect");
        return true;
    }

    @Override
    public String getEffectDescription() {
        return description;
    }
}
