package me.zodiakk.zcore.items.effects;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import me.zodiakk.zcore.items.config.CustomItemsConfig;
import me.zodiakk.zcore.items.config.CustomItemConfig;
import me.zodiakk.zcore.items.effects.impl.PermanentEffect;

public class EffectTask implements Runnable {

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            PlayerInventory inv = player.getInventory();

            List<ItemStack> contents = Arrays.asList(
                inv.getHelmet(),
                inv.getChestplate(),
                inv.getLeggings(),
                inv.getBoots(),
                inv.getItemInHand()
            );

            for (ItemStack item : contents) {
                if (item == null || !item.hasItemMeta()) {
                    continue;
                }

                CustomItemConfig config = CustomItemsConfig.getCustomItemConfig(item);

                if (config == null || !config.hasTaskEffects()) {
                    continue;
                }

                Stream<PermanentEffect> permanentEffects = config.getTaskEffects(EffectType.PERMANENT_EFFECT, PermanentEffect.class);

                permanentEffects.forEach(effect -> effect.applyEffect(player, item));
            }
        }
    }
}
