package me.zodiakk.zcore.items.effects;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface TaskEffect {
    public EffectType getEffectType();

    public String getEffectDescription();

    public void applyEffect(Player _player, ItemStack _item);
}
