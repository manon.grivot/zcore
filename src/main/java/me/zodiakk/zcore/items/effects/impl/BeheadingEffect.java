package me.zodiakk.zcore.items.effects.impl;

import com.google.gson.JsonObject;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.util.ItemUtil;
import me.zodiakk.zapi.util.RandomUtil;
import me.zodiakk.zcore.items.effects.EffectType;
import me.zodiakk.zcore.items.effects.EventEffect;
import me.zodiakk.zcore.items.util.DescriptionUtil;

public class BeheadingEffect implements EventEffect {
    Double chance;
    String description;

    public BeheadingEffect(JsonObject json) {
        this.chance = json.has("chance") ? json.get("chance").getAsDouble() : 1.0d;

        this.description = new String("&3Décapitation du joueur tué" + (chance < 1.0d ? " &6" + DescriptionUtil.getPercentage(chance) + "%" : ""));
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.BEHEADING;
    }

    @Override
    public boolean applyEffect(Event _event, ItemStack _item) {
        PlayerDeathEvent event = (PlayerDeathEvent) _event;

        if (!RandomUtil.testChance(chance)) {
            return false;
        }

        Player deadPlayer = event.getEntity();
        ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1);
        SkullMeta sm = (SkullMeta) ItemUtil.getItemMeta(skull);

        skull.setDurability((short) 3);
        sm.setOwner(deadPlayer.getName());
        skull.setItemMeta(sm);
        deadPlayer.getLocation().getWorld().dropItemNaturally(deadPlayer.getLocation(), skull);

        ZApi.logDebug("Applied beheading effect");

        return true;
    }

    @Override
    public String getEffectDescription() {
        return description;
    }
}
