package me.zodiakk.zcore.items.effects.impl;

import com.google.gson.JsonObject;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.util.RandomUtil;
import me.zodiakk.zcore.items.effects.EffectType;
import me.zodiakk.zcore.items.effects.EventEffect;
import me.zodiakk.zcore.items.util.DescriptionUtil;

public class LessDamageEffect implements EventEffect {
    Double amount;
    Double chance;
    String description;

    public LessDamageEffect(JsonObject json) {
        this.amount = json.has("amount") ? json.get("amount").getAsDouble() : 1.0d;
        this.chance = json.has("chance") ? json.get("chance").getAsDouble() : 1.0d;
        this.description = new String("&6" + DescriptionUtil.getPercentage(amount) + "% &3de réduction des dégats subis" + DescriptionUtil.getChance(chance));
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.LESS_DAMAGE;
    }

    @Override
    public boolean applyEffect(Event _event, ItemStack _item) {
        EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) _event;

        if (!(event.getEntity() instanceof Player)
                || !RandomUtil.testChance(chance)) {
            return false;
        }

        event.setDamage(event.getDamage() - event.getDamage() * amount);

        ZApi.logDebug("Applied less damage effect");
        return true;
    }

    @Override
    public String getEffectDescription() {
        return description;
    }
}
