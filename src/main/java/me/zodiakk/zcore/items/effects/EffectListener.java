package me.zodiakk.zcore.items.effects;

import java.util.HashSet;
import java.util.stream.Stream;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.zodiakk.zcore.items.config.CustomItemConfig;
import me.zodiakk.zcore.items.config.CustomItemsConfig;
import me.zodiakk.zcore.items.effects.impl.BagEffect;

public class EffectListener implements Listener {
    private static HashSet<Action> ACTION_TYPES;

    static {
        ACTION_TYPES = new HashSet<Action>();

        ACTION_TYPES.add(Action.RIGHT_CLICK_AIR);
        ACTION_TYPES.add(Action.RIGHT_CLICK_BLOCK);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        ItemStack item = event.getItem();

        if (item == null || !item.getType().equals(Material.SKULL_ITEM) || !ACTION_TYPES.contains(event.getAction())) {
            return;
        }

        CustomItemConfig config = CustomItemsConfig.getCustomItemConfig(item);

        if (config == null || !config.hasEventEffects()) {
            return;
        }

        Stream<BagEffect> bagEffects = config.getEventEffects(EffectType.BAG, BagEffect.class);

        bagEffects.forEach(effect -> effect.applyEffect(event, item));
    }
}
