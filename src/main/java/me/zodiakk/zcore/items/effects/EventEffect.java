package me.zodiakk.zcore.items.effects;

import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

public interface EventEffect {
    public EffectType getEffectType();

    public boolean applyEffect(Event _event, ItemStack _item);

    public String getEffectDescription();
}
