package me.zodiakk.zcore.items.effects.impl;

import com.google.gson.JsonObject;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.zodiakk.zcore.items.effects.EffectType;
import me.zodiakk.zcore.items.effects.TaskEffect;
import me.zodiakk.zcore.items.util.DescriptionUtil;

public class PermanentEffect implements TaskEffect {
    PotionEffectType id;
    Integer level;
    String description;

    public PermanentEffect(JsonObject json) {
        this.id = PotionEffectType.getByName(json.get("id").getAsString());
        this.level = json.has("level") ? json.get("level").getAsInt() : 1;
        this.description = new String("&3Effet permanent : &6" + DescriptionUtil.getPotionEffect(id, level, null));

        if (level <= 0) {
            level = 1;
        }
    }

    @Override
    public void applyEffect(Player _player, ItemStack _item) {
        if (_player.hasPotionEffect(id)) {
            _player.removePotionEffect(id);
        }
        _player.addPotionEffect(new PotionEffect(id, 80, this.level.intValue() - 1), true);
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.PERMANENT_EFFECT;
    }

    @Override
    public String getEffectDescription() {
        return description;
    }
}
