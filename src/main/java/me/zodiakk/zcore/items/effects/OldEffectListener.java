package me.zodiakk.zcore.items.effects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Stream;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import me.zodiakk.zcore.items.config.CustomItemConfig;
import me.zodiakk.zcore.items.config.CustomItemsConfig;
import me.zodiakk.zcore.items.effects.impl.BagEffect;
import me.zodiakk.zcore.items.effects.impl.BeheadingEffect;
import me.zodiakk.zcore.items.effects.impl.DoubleHitEffect;
import me.zodiakk.zcore.items.effects.impl.EatEffect;
import me.zodiakk.zcore.items.effects.impl.EffectToEnemy;
import me.zodiakk.zcore.items.effects.impl.EffectToOneself;
import me.zodiakk.zcore.items.effects.impl.ExpBoostBlocksEffect;
import me.zodiakk.zcore.items.effects.impl.ExpBoostEntityEffect;
import me.zodiakk.zcore.items.effects.impl.LessDamageEffect;
import me.zodiakk.zcore.items.effects.impl.LifeStealingEffect;
import me.zodiakk.zcore.items.effects.impl.SendDamageBackEffect;

// !! INVALID CLASS !!
// THIS CLASS HAS BEEN DISABLED AS CUSTOM ITEMS HAVE BEEN DISABLED
public class OldEffectListener implements Listener {
    private static HashSet<Action> ACTION_TYPES;

    static {
        ACTION_TYPES = new HashSet<Action>();

        ACTION_TYPES.add(Action.RIGHT_CLICK_AIR);
        ACTION_TYPES.add(Action.RIGHT_CLICK_BLOCK);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (!(event.getEntity() instanceof LivingEntity)) {
            return;
        }

        Entity damager = event.getDamager();
        List<ItemStack> damagerEquipment;

        LivingEntity damaged = (LivingEntity) event.getEntity();
        List<ItemStack> damagedEquipment;

        if (damager instanceof LivingEntity) {
            LivingEntity damagerLiving = (LivingEntity) damager;
            EntityEquipment eq = damagerLiving.getEquipment();

            damagerEquipment = Arrays.asList(
                eq.getHelmet(),
                eq.getChestplate(),
                eq.getLeggings(),
                eq.getBoots(),
                eq.getItemInHand()
            );
        } else {
            damagerEquipment = new ArrayList<ItemStack>(0);
        }

        {
            EntityEquipment eq = damaged.getEquipment();

            damagedEquipment = Arrays.asList(
                eq.getHelmet(),
                eq.getChestplate(),
                eq.getLeggings(),
                eq.getBoots(),
                eq.getItemInHand()
            );
        }

        for (ItemStack item : damagerEquipment) {
            if (item == null || item.getType().equals(Material.AIR) || !item.hasItemMeta()) {
                continue;
            }

            CustomItemConfig config = CustomItemsConfig.getCustomItemConfig(item);

            if (config == null || !config.hasEventEffects()) {
                continue;
            }

            Stream<EffectToEnemy> effectsToEnemy = config.getEventEffects(EffectType.EFFECT_TO_ENEMY, EffectToEnemy.class);
            effectsToEnemy.forEach(effect -> effect.applyEffect(event, item));

            Stream<EffectToOneself> effectsToOneself = config.getEventEffects(EffectType.EFFECT_TO_ONESELF, EffectToOneself.class);
            effectsToOneself.forEach(effect -> effect.applyEffect(event, item));

            Stream<LifeStealingEffect> lifeStealingEffect = config.getEventEffects(EffectType.LIFE_STEALING, LifeStealingEffect.class);
            lifeStealingEffect.forEach(effect -> effect.applyEffect(event, item));

            Stream<DoubleHitEffect> doubleHitEffects = config.getEventEffects(EffectType.DOUBLE_HIT, DoubleHitEffect.class);
            doubleHitEffects.forEach(effect -> effect.applyEffect(event, item));
        }

        for (ItemStack item : damagedEquipment) {
            if (item == null || item.getType().equals(Material.AIR) || !item.hasItemMeta()) {
                continue;
            }

            CustomItemConfig config = CustomItemsConfig.getCustomItemConfig(item);

            if (config == null || !config.hasEventEffects()) {
                continue;
            }

            Stream<SendDamageBackEffect> sendDamageBackEffects = config.getEventEffects(EffectType.SEND_DAMAGE_BACK, SendDamageBackEffect.class);
            Stream<LessDamageEffect> lessDamageEffects = config.getEventEffects(EffectType.LESS_DAMAGE, LessDamageEffect.class);

            sendDamageBackEffects.forEach(effect -> effect.applyEffect(event, item));
            lessDamageEffects.forEach(effect -> effect.applyEffect(event, item));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDeath(EntityDeathEvent event) {
        Player killer = event.getEntity().getKiller();

        if (killer == null) {
            return;
        }

        EntityEquipment eq = killer.getEquipment();
        List<ItemStack> killerEquipment = Arrays.asList(
            eq.getHelmet(),
            eq.getChestplate(),
            eq.getLeggings(),
            eq.getBoots(),
            eq.getItemInHand()
        );

        for (ItemStack item : killerEquipment) {
            if (item == null || item.getType().equals(Material.AIR) || !item.hasItemMeta()) {
                continue;
            }

            CustomItemConfig config = CustomItemsConfig.getCustomItemConfig(item);

            if (config == null || !config.hasEventEffects()) {
                continue;
            }

            Stream<ExpBoostEntityEffect> expBoostEntityEffect = config.getEventEffects(EffectType.EXP_BOOST_ENTITIES, ExpBoostEntityEffect.class);

            expBoostEntityEffect.forEach(effect -> effect.applyEffect(event, item));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player killer = event.getEntity().getKiller();
        List<ItemStack> killerEquipment;

        if (killer != null) {
            EntityEquipment eq = killer.getEquipment();
            killerEquipment = Arrays.asList(
                eq.getItemInHand()
            );
        } else {
            killerEquipment = new ArrayList<ItemStack>(0);
        }

        for (ItemStack item : killerEquipment) {
            if (item == null || item.getType().equals(Material.AIR) || !item.hasItemMeta()) {
                continue;
            }

            CustomItemConfig config = CustomItemsConfig.getCustomItemConfig(item);

            if (config == null || !config.hasEventEffects()) {
                continue;
            }

            Stream<BeheadingEffect> beheadingEffects = config.getEventEffects(EffectType.BEHEADING, BeheadingEffect.class);

            beheadingEffects.forEach(effect -> effect.applyEffect(event, item));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onItemConsume(PlayerItemConsumeEvent event) {
        if (event.isCancelled()) {
            return;
        }

        ItemStack item = event.getItem();

        if (item == null) {
            return;
        }

        CustomItemConfig config = CustomItemsConfig.getCustomItemConfig(item);

        if (config == null || !config.hasEventEffects()) {
            return;
        }

        Stream<EatEffect> eatEffects = config.getEventEffects(EffectType.EAT_EFFECT, EatEffect.class);

        eatEffects.forEach(effect -> effect.applyEffect(event, item));
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.isCancelled()) {
            return;
        }

        Player player = event.getPlayer();

        if (player == null) {
            return;
        }

        PlayerInventory inv = player.getInventory();

        List<ItemStack> items = Arrays.asList(
            inv.getHelmet(),
            inv.getChestplate(),
            inv.getLeggings(),
            inv.getBoots(),
            inv.getItemInHand()
        );

        for (ItemStack item : items) {
            if (item == null) {
                continue;
            }

            CustomItemConfig config = CustomItemsConfig.getCustomItemConfig(item);

            if (config == null || !config.hasEventEffects()) {
                continue;
            }

            Stream<ExpBoostBlocksEffect> expBoostBlocksEffects = config.getEventEffects(EffectType.EXP_BOOST_BLOCKS, ExpBoostBlocksEffect.class);

            expBoostBlocksEffects.forEach(effect -> effect.applyEffect(event, item));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        ItemStack item = event.getItem();

        if (item == null || !item.getType().equals(Material.SKULL_ITEM) || !ACTION_TYPES.contains(event.getAction())) {
            return;
        }

        CustomItemConfig config = CustomItemsConfig.getCustomItemConfig(item);

        if (config == null || !config.hasEventEffects()) {
            return;
        }

        Stream<BagEffect> bagEffects = config.getEventEffects(EffectType.BAG, BagEffect.class);

        bagEffects.forEach(effect -> effect.applyEffect(event, item));
    }
}
