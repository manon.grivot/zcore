package me.zodiakk.zcore.items.effects.impl;

import com.google.gson.JsonObject;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.zodiakk.zapi.config.types.RewardList;
import me.zodiakk.zcore.items.effects.EffectType;
import me.zodiakk.zcore.items.effects.EventEffect;

public class BagEffect implements EventEffect {
    RewardList rewards;

    public BagEffect(JsonObject json) {
        this.rewards = new RewardList(json.getAsJsonArray("rewardList"));
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.BAG;
    }

    @Override
    public boolean applyEffect(Event _event, ItemStack _item) {
        PlayerInteractEvent event = (PlayerInteractEvent) _event;
        Player player = event.getPlayer();
        ItemStack item = player.getInventory().getItemInHand();

        rewards.rewardPlayer(player);
        player.playSound(player.getLocation(), Sound.HORSE_ARMOR, 100.0f, 1.2f);
        if (item.getAmount() <= 1) {
            player.getInventory().removeItem(item);
        } else {
            item.setAmount(item.getAmount() - 1);
        }
        event.setCancelled(true);
        return true;
    }

    @Override
    public String getEffectDescription() {
        return null;
    }
}
