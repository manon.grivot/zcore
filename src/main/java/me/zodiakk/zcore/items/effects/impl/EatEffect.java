package me.zodiakk.zcore.items.effects.impl;

import com.google.gson.JsonObject;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.util.RandomUtil;
import me.zodiakk.zcore.items.effects.EffectType;
import me.zodiakk.zcore.items.effects.EventEffect;
import me.zodiakk.zcore.items.util.DescriptionUtil;

public class EatEffect implements EventEffect {
    PotionEffectType id;
    Integer duration;
    Integer level;
    Double chance;
    String description;

    public EatEffect(JsonObject json) {
        this.id = PotionEffectType.getByName(json.get("id").getAsString());
        this.duration = json.get("duration").getAsInt();
        this.level = json.has("level") ? json.get("level").getAsInt() : 1;
        this.chance = json.has("chance") ? json.get("chance").getAsDouble() : 1.0d;
        this.description = new String("&3Effet lorsque l'objet est consommé : &6"
                + DescriptionUtil.getPotionEffect(id, level, duration) + DescriptionUtil.getChance(chance));
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.EAT_EFFECT;
    }

    @Override
    public boolean applyEffect(Event _event, ItemStack _item) {
        PlayerItemConsumeEvent event = (PlayerItemConsumeEvent) _event;

        if (!RandomUtil.testChance(chance)) {
            return false;
        }

        Player player = event.getPlayer();
        if (player.hasPotionEffect(id)) {
            player.removePotionEffect(id);
        }
        player.addPotionEffect(new PotionEffect(id, duration.intValue() * 20, this.level.intValue() - 1), true);

        ZApi.logDebug("Applied eat effect effect");
        return true;
    }

    @Override
    public String getEffectDescription() {
        return description;
    }
}
