package me.zodiakk.zcore.items.effects.impl;

import com.google.gson.JsonObject;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.util.RandomUtil;
import me.zodiakk.zcore.items.effects.EffectType;
import me.zodiakk.zcore.items.effects.EventEffect;
import me.zodiakk.zcore.items.util.DescriptionUtil;

public class LifeStealingEffect implements EventEffect {
    Double amount;
    Double chance;
    String description;

    public LifeStealingEffect(JsonObject json) {
        this.amount = json.has("amount") ? json.get("amount").getAsDouble() : 1.0d;
        this.chance = json.has("chance") ? json.get("chance").getAsDouble() : 1.0d;
        this.description = new String("&6" + DescriptionUtil.getPercentage(amount)
                + "% &3des dégats infligés retournés en PV" + DescriptionUtil.getChance(chance));
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.LIFE_STEALING;
    }

    @Override
    public boolean applyEffect(Event _event, ItemStack _item) {
        EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) _event;

        if (!(event.getDamager() instanceof Player)
                || !RandomUtil.testChance(chance)) {
            return false;
        }

        Player player = (Player) event.getDamager();
        Double newHealth = player.getHealth() + (event.getDamage() * amount);
        Double maxHealth = player.getMaxHealth();

        if (newHealth > maxHealth) {
            player.setHealth(maxHealth);
        } else {
            player.setHealth(newHealth);
        }

        ZApi.logDebug("Applied life stealing effect");
        return true;
    }

    @Override
    public String getEffectDescription() {
        return description;
    }
}
