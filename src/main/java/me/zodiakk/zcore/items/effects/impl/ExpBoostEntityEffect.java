package me.zodiakk.zcore.items.effects.impl;

import com.google.gson.JsonObject;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zcore.items.effects.EffectType;
import me.zodiakk.zcore.items.effects.EventEffect;
import me.zodiakk.zcore.items.util.DescriptionUtil;

public class ExpBoostEntityEffect implements EventEffect {
    Double amount;
    String description;

    public ExpBoostEntityEffect(JsonObject json) {
        this.amount = json.has("amount") ? json.get("amount").getAsDouble() : 1.0d;
        this.description = new String("&3Boost d'expérience sur les mobs : &6" + DescriptionUtil.getPercentage(amount) + "%");
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.EXP_BOOST_ENTITIES;
    }

    @Override
    public boolean applyEffect(Event _event, ItemStack _item) {
        EntityDeathEvent event = (EntityDeathEvent) _event;

        if (!(event.getEntity() instanceof LivingEntity)
                || !(event.getEntity().getKiller() != null)
                || event.getEntity() instanceof Player) {
            return false;
        }

        event.setDroppedExp(Double.valueOf(event.getDroppedExp() * amount).intValue());

        ZApi.logDebug("Applied exp boost entities effect");
        return true;
    }

    @Override
    public String getEffectDescription() {
        return description;
    }
}
