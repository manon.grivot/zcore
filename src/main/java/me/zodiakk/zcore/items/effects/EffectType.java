package me.zodiakk.zcore.items.effects;

public enum EffectType {
    PERMANENT_EFFECT(Category.TASK),
    EFFECT_TO_ENEMY(Category.EVENT),
    EFFECT_TO_ONESELF(Category.EVENT),
    EAT_EFFECT(Category.EVENT),
    EXP_BOOST_ENTITIES(Category.EVENT),
    EXP_BOOST_BLOCKS(Category.EVENT),
    BEHEADING(Category.EVENT),
    LIFE_STEALING(Category.EVENT),
    SEND_DAMAGE_BACK(Category.EVENT),
    DOUBLE_HIT(Category.EVENT),
    LESS_DAMAGE(Category.EVENT),
    BAG(Category.EVENT),
    GENERIC(null);

    private Category cat;

    private EffectType(Category cat) {
        this.cat = cat;
    }

    public boolean isTask() {
        return cat.equals(Category.TASK);
    }

    public boolean isEvent() {
        return cat.equals(Category.EVENT);
    }

    private enum Category {
        EVENT,
        TASK,
    }
}
