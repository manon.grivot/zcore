package me.zodiakk.zcore.items.effects.impl;

import com.google.gson.JsonObject;

import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zcore.items.effects.EffectType;
import me.zodiakk.zcore.items.effects.EventEffect;
import me.zodiakk.zcore.items.util.DescriptionUtil;

public class ExpBoostBlocksEffect implements EventEffect {
    Double amount;
    String description;

    public ExpBoostBlocksEffect(JsonObject json) {
        this.amount = json.has("amount") ? json.get("amount").getAsDouble() : 1.0d;
        this.description = new String("&3Boost d'expérience sur les blocs : &6+" + DescriptionUtil.getPercentage(amount) + "%");
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.EXP_BOOST_BLOCKS;
    }

    @Override
    public boolean applyEffect(Event _event, ItemStack _item) {
        BlockBreakEvent event = (BlockBreakEvent) _event;

        event.setExpToDrop(Double.valueOf(event.getExpToDrop() * (amount + 1.0d)).intValue());

        ZApi.logDebug("Applied exp boost blocks effect");
        return true;
    }

    @Override
    public String getEffectDescription() {
        return description;
    }
}
