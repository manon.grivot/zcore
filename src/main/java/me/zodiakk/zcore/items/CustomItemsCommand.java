package me.zodiakk.zcore.items;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.zodiakk.zcore.items.config.CustomItemsConfig;
import me.zodiakk.zcore.items.config.CustomItemConfig;

public class CustomItemsCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage("§cYou must be a player to run this command.");
            return true;
        }

        if (args.length == 0) {
            sendHelp(sender);
            return true;
        }

        if (args[0].equalsIgnoreCase("give") && args.length >= 2 && args.length <= 3) {
            // Give item

            Player player;

            if (args.length >= 3) {
                player = Bukkit.getPlayer(args[2]);
            } else {
                player = (Player) sender;
            }

            if (player == null) {
                sender.sendMessage("§cPlayer does not exists.");
                return true;
            }

            CustomItemConfig config = CustomItemsConfig.getCustomItemConfig(args[1]);

            if (config == null) {
                sender.sendMessage("§cItem key does not exists.");
                return true;
            }

            player.getInventory().addItem(config.getClonedItemStack());
            return true;
        } else if (args[0].equalsIgnoreCase("infos") && args.length == 1) {
            // Item infos

            CustomItemConfig config = CustomItemsConfig.getCustomItemConfig(((Player) sender).getItemInHand());

            if (config == null) {
                sender.sendMessage("§cCurrently held item is not a custom item.");
                return true;
            }

            sender.sendMessage("§akey: " + config.getKey());
            sender.sendMessage("§cevent effects:");
            config.getEventEffects().forEach(effect -> sender.sendMessage("§a" + effect.getEffectType().toString().toLowerCase()));
            sender.sendMessage("§ctask effects:");
            config.getTaskEffects().forEach(effect -> sender.sendMessage("§a" + effect.getEffectType().toString().toLowerCase()));
            return true;
        }

        sendHelp(sender);

        return true;
    }

    public void sendHelp(CommandSender sender) {
        sender.sendMessage("§c/hci give <id> <player>");
        sender.sendMessage("§c/hci infos");
    }
}
