package me.zodiakk.zcore.items;

import java.util.Arrays;
import java.util.List;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.zodiakk.zcore.items.config.CustomItemsConfig;

public class CustomItemsListener implements Listener {
    private static List<InventoryType> CANCELLED_INVENTORY_TYPES;

    static {
        CANCELLED_INVENTORY_TYPES = Arrays.asList(
            InventoryType.ANVIL,
            InventoryType.CRAFTING,
            InventoryType.WORKBENCH,
            InventoryType.ENCHANTING
        );
    }

    @EventHandler
    public void onInventoryMoveItem(InventoryMoveItemEvent event) {
        // Cancel any modifications with custom items
        Inventory destination = event.getDestination();
        ItemStack item = event.getItem();

        if (CANCELLED_INVENTORY_TYPES.contains(destination.getType())) {
            if (CustomItemsConfig.getCustomItemConfig(item) != null) {
                event.setCancelled(true);
            }
        }
    }
}
