package me.zodiakk.zcore.items.config;

import java.io.File;
import java.util.HashSet;

import org.bukkit.inventory.ItemStack;

import me.zodiakk.zapi.util.ItemUtil;
import me.zodiakk.zapi.util.JsonUtil;
import me.zodiakk.zcore.ZCore;
import me.zodiakk.zcore.ZCoreLogger;

public class CustomItemsConfig {
    private static HashSet<CustomItemConfig> items;

    public CustomItemsConfig(ZCore core, File itemsDirectory) {
        items = new HashSet<CustomItemConfig>();

        if (itemsDirectory.isFile()) {
            itemsDirectory.delete();
        }
        itemsDirectory.mkdirs();
        for (File itemFile : itemsDirectory.listFiles()) {
            try {
                items.add(new CustomItemConfig(JsonUtil.readJsonFile(itemFile).getAsJsonObject()));
            } catch (Exception ex) {
                ZCoreLogger.warn("CustomItems", "Could not load the custom item represented by the " + itemFile.getName() + " file.");
            }
        }
    }

    public static CustomItemConfig getCustomItemConfig(String key) {
        for (CustomItemConfig item : items) {
            if (item.getKey().equals(key)) {
                return item;
            }
        }
        return null;
    }

    public static CustomItemConfig getCustomItemConfig(ItemStack item) {
        try {
            return getCustomItemConfig((String) ItemUtil.getNBTTag(item, "custom_id", String.class));
        } catch (Exception ex) {
            return null;
        }
    }
}
