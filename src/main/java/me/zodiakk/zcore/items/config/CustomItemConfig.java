package me.zodiakk.zcore.items.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Stream;

import com.google.gson.JsonObject;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.util.Attributes;
import me.zodiakk.zapi.util.Attributes.Attribute;
import me.zodiakk.zapi.util.Attributes.AttributeType;
import me.zodiakk.zapi.util.ItemUtil;
import me.zodiakk.zapi.util.JsonUtil;
import me.zodiakk.zcore.items.effects.EffectType;
import me.zodiakk.zcore.items.effects.EventEffect;
import me.zodiakk.zcore.items.effects.TaskEffect;
import me.zodiakk.zcore.items.effects.impl.BagEffect;
import me.zodiakk.zcore.items.effects.impl.BeheadingEffect;
import me.zodiakk.zcore.items.effects.impl.DoubleHitEffect;
import me.zodiakk.zcore.items.effects.impl.EatEffect;
import me.zodiakk.zcore.items.effects.impl.EffectToEnemy;
import me.zodiakk.zcore.items.effects.impl.EffectToOneself;
import me.zodiakk.zcore.items.effects.impl.ExpBoostBlocksEffect;
import me.zodiakk.zcore.items.effects.impl.ExpBoostEntityEffect;
import me.zodiakk.zcore.items.effects.impl.LessDamageEffect;
import me.zodiakk.zcore.items.effects.impl.LifeStealingEffect;
import me.zodiakk.zcore.items.effects.impl.PermanentEffect;
import me.zodiakk.zcore.items.effects.impl.SendDamageBackEffect;
import me.zodiakk.zcore.items.util.DescriptionUtil;

public class CustomItemConfig {
    private String key;
    private List<EventEffect> eventEffects;
    private List<TaskEffect> taskEffects;
    private ItemStack stack;

    public CustomItemConfig(JsonObject object) {
        this.key = object.get("custom_id").getAsString();
        this.eventEffects = new ArrayList<EventEffect>();
        this.taskEffects = new ArrayList<TaskEffect>();

        final List<String> attributesDesc = new ArrayList<String>();
        final List<String> effectsDesc = new ArrayList<String>();
        final List<String> enchantsDesc = new ArrayList<String>();

        stack = JsonUtil.itemStackFromJson(object.get("stack").getAsJsonObject());
        ItemMeta stackMeta = stack.getItemMeta();
        Attributes stackAttributes = new Attributes(stack);
        Double currentAttackDamage = DescriptionUtil.getAttackDamage(stack.getType());

        // Item attributes description
        if (currentAttackDamage.intValue() != 1 && stackAttributes.size() > 0) {
            for (Attribute attribute : stackAttributes.values()) {
                if (attribute.getAttributeType().equals(AttributeType.GENERIC_KNOCKBACK_RESISTANCE)) {
                    attributesDesc.add("&7Résistance au knockback &c" + DescriptionUtil.getAttributesText(attribute.getOperation(), attribute.getAmount()));
                } else if (attribute.getAttributeType().equals(AttributeType.GENERIC_MAX_HEALTH)) {
                    attributesDesc.add("&c" + DescriptionUtil.getAttributesText(attribute.getOperation(), attribute.getAmount()) + " &7point" + (attribute.getAmount() == 1 ? "" : "s") + " de vie");
                } else if (attribute.getAttributeType().equals(AttributeType.GENERIC_ATTACK_DAMAGE)) {
                    currentAttackDamage += attribute.getAmount();
                }
            }
        }

        // Item enchantments
        if (stackMeta.hasEnchants()) {
            for (Entry<Enchantment, Integer> enchant : stackMeta.getEnchants().entrySet()) {
                enchantsDesc.add(DescriptionUtil.getEnchantmentDescription(enchant.getKey(), enchant.getValue()));

                if (enchant.getKey().equals(Enchantment.DAMAGE_ALL)) {
                    currentAttackDamage += 1.25 * enchant.getValue().doubleValue();
                }
            }
        }

        // Item effects
        object.get("effects").getAsJsonArray().forEach(effect -> {
            JsonObject effectObject = effect.getAsJsonObject();
            String key = effectObject.get("key").getAsString();

            EventEffect eventEffect = getEventEffect(key, effectObject);

            if (eventEffect != null) {
                String effectDescription = eventEffect.getEffectDescription();
                eventEffects.add(eventEffect);

                if (effectDescription != null && effectDescription.length() > 0) {
                    effectsDesc.add(effectDescription);
                }
                return;
            }

            TaskEffect taskEffect = getTaskEffect(key, effectObject);

            if (taskEffect != null) {
                taskEffects.add(taskEffect);
                effectsDesc.add(taskEffect.getEffectDescription());
                return;
            }
        });

        // Build item description
        List<String> lore = stackMeta.getLore();

        lore.add("");
        lore.addAll(effectsDesc);
        lore.addAll(enchantsDesc);
        lore.add("");
        lore.add("§7Dégats : " + Double.valueOf(currentAttackDamage * 100).intValue() / 100.0d + " PV");
        lore.addAll(attributesDesc);

        // Line splitting
        List<String> finalLore = new ArrayList<String>();

        for (String str : lore) {
            finalLore.addAll(ItemUtil.parseLoreLine(str));
        }

        stackMeta.setLore(finalLore);
        stack.setItemMeta(stackMeta);
        stack.setAmount(1);

        if (stackMeta.getDisplayName() == null) {
            ZApi.logDebug("Loaded custom item: " + key);
        } else {
            ZApi.logDebug("Loaded custom item: " + stackMeta.getDisplayName().replaceAll("[&§][0-9a-fA-F]", ""));
        }
    }

    public String getKey() {
        return key;
    }

    public boolean hasEventEffects() {
        return eventEffects.size() > 0;
    }

    public List<EventEffect> getEventEffects() {
        return eventEffects;
    }

    public <T extends EventEffect> Stream<T> getEventEffects(EffectType type, Class<T> clazz) {
        return eventEffects.stream().filter(eff -> eff.getEffectType().equals(type)).map(elem -> clazz.cast(elem));
    }

    public boolean hasTaskEffects() {
        return taskEffects.size() > 0;
    }

    public List<TaskEffect> getTaskEffects() {
        return taskEffects;
    }

    public <T extends TaskEffect> Stream<T> getTaskEffects(EffectType type, Class<T> clazz) {
        return taskEffects.stream().filter(eff -> eff.getEffectType().equals(type)).map(elem -> clazz.cast(elem));
    }

    public ItemStack getItemStack() {
        return stack;
    }

    public ItemStack getClonedItemStack() {
        return stack.clone();
    }

    private static EventEffect getEventEffect(String key, JsonObject values) {
        switch (key.toLowerCase()) {
        case "bag":
            return new BagEffect(values);
        case "beheading":
            return new BeheadingEffect(values);
        case "double_hit":
            return new DoubleHitEffect(values);
        case "eat_effect":
            return new EatEffect(values);
        case "effect_to_enemy":
            return new EffectToEnemy(values);
        case "effect_to_oneself":
            return new EffectToOneself(values);
        case "exp_boost_blocks":
            return new ExpBoostBlocksEffect(values);
        case "exp_boost_entities":
            return new ExpBoostEntityEffect(values);
        case "less_damage":
            return new LessDamageEffect(values);
        case "life_stealing":
            return new LifeStealingEffect(values);
        case "send_damage_back":
            return new SendDamageBackEffect(values);
        default:
            return null;
        }
    }

    private static TaskEffect getTaskEffect(String key, JsonObject values) {
        switch (key.toLowerCase()) {
        case "permanent_effect":
            return new PermanentEffect(values);
        default:
            return null;
        }
    }
}
