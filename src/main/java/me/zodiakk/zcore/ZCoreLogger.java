package me.zodiakk.zcore;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import me.zodiakk.zapi.ZApi;

public final class ZCoreLogger {
    private static Logger logger;

    static {
        logger = Bukkit.getLogger();
    }

    private ZCoreLogger() {
    }

    public static void log(String message) {
        logger.info(message);
    }

    public static void log(String module, String message) {
        logger.info(module + ": " + message);
    }

    public static void warn(String message) {
        logger.warning(message);
    }

    public static void warn(String module, String message) {
        logger.warning(module + ": " + message);
    }

    public static void error(String message) {
        logger.severe(message);
    }

    public static void error(String module, String message) {
        logger.severe(module + ": " + message);
    }

    public static void error(Exception exception) {
        ZApi.postError(exception);
    }

    public static void error(Exception exception, OfflinePlayer player) {
        ZApi.postError(exception, player);
    }

    public static void error(String module, Exception exception) {
        logger.severe("Error within the " + module + " module:");
        ZApi.postError(exception);
    }

    public static void error(String module, Exception exception, OfflinePlayer player) {
        logger.severe("Error within the " + module + " module:");
        ZApi.postError(exception, player);
    }

    public static void debug(String message) {
        if (ZCoreConfig.getInstance().getDebug()) {
            logger.info("(DEBUG) " + message);
        }
    }

    public static void debug(String module, String message) {
        if (ZCoreConfig.getInstance().getDebug()) {
            logger.info("(DEBUG) " + module + ": " + message);
        }
    }
}
