package me.zodiakk.zcore.coinflip;

import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.data.filter.WhereFilter;
import me.zodiakk.zapi.data.filter.WhereOperator;
import me.zodiakk.zapi.data.filter.WhereOperatorType;
import me.zodiakk.zapi.data.result.Result;
import me.zodiakk.zapi.data.table.TableModel;
import me.zodiakk.zapi.data.table.TableRow;

public class CoinflipDataListener implements Listener {

    private TableModel stats;

    public CoinflipDataListener() {
        this.stats = ZApi.getData().getTableManager().getModel("stat_coinflip");
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        ZApi.getThreadsManager().executeInMainThread(new Runnable() {
            @Override
            public void run() {
                UUID uuid = event.getPlayer().getUniqueId();
                TableRow player = getPlayerRow(uuid);

                if (player == null) {
                    event.getPlayer().kickPlayer(ChatColor.RED + "Une erreur est survenue lors de votre connexion.\n" + ChatColor.RED + "Si le problème persiste, merci de contacter un administrateur.");
                    return;
                }
            }
        });
    }

    private TableRow getPlayerRow(UUID uuid) {
        try {
            WhereFilter queryWhere = new WhereFilter();
            Result select;

            queryWhere.addOperator(new WhereOperator("uuid", WhereOperatorType.EQ, uuid.toString()));
            select = stats.select(queryWhere);
            if (select.getResults().size() == 0) {
                TableRow player = new TableRow(stats);

                player.setValue("uuid", uuid.toString());
                stats.insert(player);
                return getPlayerRow(uuid);
            }
            return select.getResults().get(0).toTableRow();
        } catch (SQLException sqle) {
            ZApi.postError(sqle, uuid);
            return null;
        }
    }
}
