package me.zodiakk.zcore.coinflip;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.event.HandlerList;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.data.table.TableField;
import me.zodiakk.zapi.data.table.TableField.DefaultValue;
import me.zodiakk.zapi.data.table.TableField.FieldType;
import me.zodiakk.zapi.data.table.TableField.IndexType;
import me.zodiakk.zapi.data.table.TableModel;
import me.zodiakk.zcore.ZCore;
import me.zodiakk.zcore.modules.AbstractModule;
import me.zodiakk.zcore.modules.ModuleStatus;

public class CoinflipModule extends AbstractModule {

    private CommandExecutor coinFlip;
    private CoinflipDataListener coinflipDataListener;
    private CoinflipPlaceholder coinflipPlaceholder;

    public CoinflipModule() {
        super("CoinFlip");
    }

    @Override
    public ModuleStatus enable(ZCore core) {
        try {
            TableModel stats = ZApi.getData().getTableManager().getModel("stat_coinflip");

            if (stats == null) {
                stats = new TableModel("stat_coinflip");
                ZApi.getData().getTableManager().setModel(stats);
            }
            stats.addField("uuid", new TableField<String>("uuid", FieldType.VARCHAR, 36, IndexType.PRIMARY));
            stats.addField("played", new TableField<Integer>("played", FieldType.INT, new DefaultValue<Integer>(0)));
            stats.addField("won", new TableField<Integer>("won", FieldType.INT, new DefaultValue<Integer>(0)));
            stats.addField("played_money", new TableField<String>("played_money", FieldType.VARCHAR, 32, new DefaultValue<String>("0.0")));
            stats.addField("won_money", new TableField<String>("won_money", FieldType.VARCHAR, 32, new DefaultValue<String>("0.0")));
        } catch (Exception e) {
            ZApi.postError(e);
            return setStatus(ModuleStatus.FAILED_TO_LOAD);
        }
        this.coinflipDataListener = new CoinflipDataListener();
        Bukkit.getServer().getPluginManager().registerEvents(coinflipDataListener, core);
        this.coinflipPlaceholder = new CoinflipPlaceholder(core);
        this.coinflipPlaceholder.register();
        this.coinFlip = new CoinflipCommand();
        core.getCommand("coinflip").setExecutor(this.coinFlip);
        return setStatus(ModuleStatus.ENABLED);
    }

    @Override
    public ModuleStatus disable(ZCore core) {
        HandlerList.unregisterAll(this.coinflipDataListener);
        core.getCommand("coinflip").setExecutor(null);
        return setStatus(ModuleStatus.DISABLED);
    }

    @Override
    public ModuleStatus reload(ZCore core) {
        return (disable(core) != ModuleStatus.DISABLED) ? getStatus() : enable(core);
    }
}
