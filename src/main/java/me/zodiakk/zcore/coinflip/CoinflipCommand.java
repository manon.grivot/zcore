package me.zodiakk.zcore.coinflip;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.commands.HelpMenu;
import me.zodiakk.zapi.data.filter.WhereFilter;
import me.zodiakk.zapi.data.filter.WhereOperator;
import me.zodiakk.zapi.data.filter.WhereOperatorType;
import me.zodiakk.zapi.data.result.Result;
import me.zodiakk.zapi.data.table.TableModel;
import me.zodiakk.zapi.data.table.TableRow;
import me.zodiakk.zapi.util.ChatSession;
import net.milkbowl.vault.economy.Economy;

public class CoinflipCommand implements CommandExecutor {

    private static Economy economy = null;
    private static Random random;
    private HashSet<CoinflipInfos> requests;
    private HashMap<UUID, Long> cooldowns;
    private TableModel stats;
    private HelpMenu help;

    static {
        random = ThreadLocalRandom.current();
    }

    public CoinflipCommand() {
        this.requests = new HashSet<CoinflipInfos>();
        this.cooldowns = new HashMap<UUID, Long>();
        this.stats = ZApi.getData().getTableManager().getModel("stat_coinflip");
        this.help = new HelpMenu("Coinflip", "cf");
        this.help.addCommand("<pseudo> <montant>", "Envoyer une demande de coinflip");
        this.help.addCommand("<pseudo> confirm", "Confirmer une demande de coinflip");
        this.help.buildMenu(6);
    }

    @Override
    public boolean onCommand(CommandSender sdr, Command command, String label, String[] args) {
        if (!(sdr instanceof Player)) {
            Bukkit.getLogger().severe("Erreur : Cette commande n'est pas censée être utilisée depuis la console.");
            return true;
        }
        if (args.length != 2) {
            this.help.sendPage(sdr, 1);
            return true;
        }
        Player sender = (Player) sdr;
        Player receiver = Bukkit.getPlayer(args[0]);
        ChatSession senderChat = new ChatSession("Coinflip", sender.getUniqueId());

        if (receiver == null || !receiver.isOnline()) {
            senderChat.send(ChatColor.RED + "Erreur : Le joueur doit être connecté.");
            return true;
        }
        ChatSession receiverChat = new ChatSession("Coinflip", receiver.getUniqueId());

        if (sender.getUniqueId().equals(receiver.getUniqueId())) {
            senderChat.send(ChatColor.RED + "Erreur : Vous ne pouvez pas faire de coinflip avec vous même.");
            return true;
        }
        if (args[1].equals("confirm")) {
            CoinflipInfos request = getCoinFlipInfos(receiver);

            if (request == null || !(request.getReceiver().equals(sender))) {
                senderChat.send(ChatColor.RED + "Erreur : Ce joueur ne vous a pas envoyé de demande de coinflip.");
                return true;
            }
            double amount = request.getAmount();

            if (getEconomy().getBalance(sender) < amount) {
                senderChat.send(ChatColor.RED + "Erreur : Vous n'avez pas assez d'argent pour confirmer ce coinflip.");
                return true;
            }
            if (getEconomy().getBalance(receiver) < amount) {
                senderChat.send(ChatColor.RED + "Erreur : " + receiver.getName() + " n'a plus assez d'argent pour ce coinflip, il a donc été annulé.");
                receiverChat.send(ChatColor.RED + "Erreur : Le coinflip a été annulé car vous n'avez plus assez d'argent.");
                removeCoinFlipInfos(request);
                return true;
            }
            if (random.nextBoolean()) {
                getEconomy().depositPlayer(sender, amount);
                getEconomy().withdrawPlayer(receiver, amount);
                updateStat(sender, receiver, amount);
                senderChat.send(ChatColor.GREEN + "Vous confirmez le coinflip, et gagnez !");
                senderChat.send(ChatColor.GREEN + "Vous remportez le double de votre mise, soit " + amount * 2 + "$ !");
                receiverChat.send(ChatColor.RED + sender.getName() + " a confirmé le coinflip, et vous perdez.");
                receiverChat.send(ChatColor.RED + "Vous perdez votre mise de " + amount + "$.");
            } else {
                getEconomy().depositPlayer(receiver, amount);
                getEconomy().withdrawPlayer(sender, amount);
                updateStat(receiver, sender, amount);
                receiverChat.send(ChatColor.GREEN + sender.getName() + " a confirmé le coinflip, et vous gagnez !");
                receiverChat.send(ChatColor.GREEN + "Vous remportez le double de votre mise, soit " + amount * 2 + "$ !");
                senderChat.send(ChatColor.RED + "Vous confirmez le coinflip, et perdez.");
                senderChat.send(ChatColor.RED + "Vous perdez votre mise de " + amount + "$.");
            }
            removeCoinFlipInfos(request);
        } else {
            double amount = -1;

            if (this.cooldowns.containsKey(sender.getUniqueId()) && this.cooldowns.get(sender.getUniqueId()) > System.currentTimeMillis()) {
                long remainingTime = ((this.cooldowns.get(sender.getUniqueId()) - System.currentTimeMillis()) / 1000) + 1;

                senderChat.send(ChatColor.RED + "Erreur : Vous devez patienter encore " + remainingTime + " secondes.");
                return true;
            }
            try {
                amount = Double.parseDouble(args[1]);
                if (amount <= 0) {
                    senderChat.send(ChatColor.RED + "Erreur : Le montant doit être un nombre positif.");
                    return true;
                }
            } catch (NumberFormatException nfe) {
                senderChat.send(ChatColor.RED + "Erreur : Le montant doit être un nombre positif.");
                return true;
            }
            if (getEconomy().getBalance(sender) < amount) {
                senderChat.send(ChatColor.RED + "Erreur : Vous n'avez pas assez d'argent pour parier un coinflip de cette valeur.");
                return true;
            }
            if (getEconomy().getBalance(receiver) < amount) {
                senderChat.send(ChatColor.RED + "Erreur : Le joueur " + receiver.getName() + " n'a pas assez d'argent pour parier un coinflip de cette valeur.");
                return true;
            }
            CoinflipInfos previousRequest = getCoinFlipInfos(sender);

            if (previousRequest != null) {
                senderChat.send(ChatColor.RED + "Votre précédent coinflip avec " + previousRequest.getReceiver().getName() + " a été annulé.");
                ChatSession.sendOne(previousRequest.getReceiver().getUniqueId(), "Coinflip", ChatColor.RED + "Le coinflip que vous avait lancé " + sender.getName() + " a été annulé.");
                removeCoinFlipInfos(previousRequest);
            }
            this.cooldowns.remove(sender.getUniqueId());
            this.cooldowns.put(sender.getUniqueId(), (System.currentTimeMillis() + 30000L));
            addCoinFlipInfos(new CoinflipInfos(sender, receiver, amount));
            senderChat.send(ChatColor.GREEN + "Vous avez parié " + amount + "$ à un coinflip contre " + receiver.getName() + ".");
            receiverChat.send(ChatColor.GREEN + sender.getName() + " a parié " + amount + "$ à un coinflip contre vous.");
            receiverChat.send(ChatColor.GREEN + "Pour l'accepter, faites \"/coinflip " + sender.getName() + " confirm\".");
        }
        return true;
    }

    private Economy getEconomy() {
        if (economy != null) {
            return economy;
        }
        RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);

        if (rsp == null) {
            return null;
        }
        return economy = rsp.getProvider();
    }

    private CoinflipInfos getCoinFlipInfos(Player first) {
        for (CoinflipInfos cfi : requests) {
            if (cfi.getSender().getUniqueId().equals(first.getUniqueId())) {
                return cfi;
            }
        }
        return null;
    }

    private void addCoinFlipInfos(CoinflipInfos cfi) {
        requests.add(cfi);
    }

    private void removeCoinFlipInfos(CoinflipInfos cfi) {
        for (CoinflipInfos infos : requests) {
            if (cfi.equals(infos)) {
                requests.remove(infos);
                return;
            }
        }
    }

    private void updateStat(Player winner, Player looser, double amount) {
        try {
            WhereFilter queryWhere = new WhereFilter();
            Result select;
            TableRow tableRow;
            final BigDecimal playedMoney;
            final BigDecimal wonMoney;

            queryWhere.addOperator(new WhereOperator("uuid", WhereOperatorType.EQ, winner.getUniqueId().toString()));
            select = stats.select(queryWhere);
            tableRow = select.getResults().get(0).toTableRow();
            playedMoney = new BigDecimal((String) (tableRow.getValue("played_money").getValue()));
            wonMoney = new BigDecimal((String) (tableRow.getValue("won_money").getValue()));
            tableRow.setValue("played", (((Integer) (tableRow.getValue("played").getValue())) + 1));
            tableRow.setValue("won", (((Integer) (tableRow.getValue("won").getValue())) + 1));
            tableRow.setValue("played_money", (playedMoney.add(new BigDecimal(amount))).setScale(3, RoundingMode.DOWN).toString());
            tableRow.setValue("won_money", (wonMoney.add(new BigDecimal(amount))).setScale(3, RoundingMode.DOWN).toString());
            stats.updateRows(tableRow);
        } catch (SQLException sqle) {
            ZApi.postError(sqle, winner.getUniqueId());
            return;
        }
        try {
            WhereFilter queryWhere = new WhereFilter();
            Result select;
            TableRow tableRow;
            final BigDecimal playedMoney;

            queryWhere.addOperator(new WhereOperator("uuid", WhereOperatorType.EQ, looser.getUniqueId().toString()));
            select = stats.select(queryWhere);
            tableRow = select.getResults().get(0).toTableRow();
            playedMoney = new BigDecimal((String) (tableRow.getValue("played_money").getValue()));
            tableRow.setValue("played", (((Integer) (tableRow.getValue("played").getValue())) + 1));
            tableRow.setValue("played_money", (playedMoney.add(new BigDecimal(amount))).setScale(3, RoundingMode.DOWN).toString());
            stats.updateRows(tableRow);
        } catch (SQLException sqle) {
            ZApi.postError(sqle, looser.getUniqueId());
            return;
        }
    }
}
