package me.zodiakk.zcore.coinflip;

import java.sql.SQLException;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.data.filter.WhereFilter;
import me.zodiakk.zapi.data.filter.WhereOperator;
import me.zodiakk.zapi.data.filter.WhereOperatorType;
import me.zodiakk.zapi.data.result.Result;

public class CoinflipPlaceholder extends PlaceholderExpansion {
    private Plugin plugin;

    public CoinflipPlaceholder(Plugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getAuthor() {
        return "Duck";
    }

    @Override
    public String getIdentifier() {
        return "zcore-coinflip";
    }

    @Override
    public String getVersion() {
        return plugin.getDescription().getVersion();
    }

    @Override
    public String onPlaceholderRequest(Player player, String identifier) {
        if (player == null) {
            return "";
        }
        if (!(identifier.equals("played") || identifier.equals("won") || identifier.equals("played-money") || identifier.equals("won-money"))) {
            return "";
        }
        WhereFilter queryWhere = new WhereFilter();
        Result select;

        try {
            queryWhere.addOperator(new WhereOperator("uuid", WhereOperatorType.EQ, player.getUniqueId().toString()));
            select = ZApi.getData().getTableManager().getModel("stat_coinflip").select(queryWhere);
        } catch (SQLException sqle) {
            ZApi.postError(sqle, player.getUniqueId());
            return "Erreur";
        }
        switch (identifier) {
        case "played":
            return select.getResults().get(0).getValue("played").getValue().toString();
        case "won":
            return select.getResults().get(0).getValue("won").getValue().toString();
        case "played-money":
            return select.getResults().get(0).getValue("played_money").getValue().toString();
        case "won-money":
            return select.getResults().get(0).getValue("won_money").getValue().toString();
        default:
            return "";
        }
    }
}
