package me.zodiakk.zcore.coinflip;

import org.bukkit.entity.Player;

public class CoinflipInfos {

    private Player sender;
    private Player receiver;
    private double amount;

    public CoinflipInfos(Player sender, Player receiver, double amount) {
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
    }

    public Player getSender() {
        return this.sender;
    }

    public Player getReceiver() {
        return this.receiver;
    }

    public double getAmount() {
        return this.amount;
    }
}
