package me.zodiakk.zcore.patches;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;

import me.zodiakk.zcore.ZCore;
import me.zodiakk.zcore.modules.AbstractModule;
import me.zodiakk.zcore.modules.ModuleStatus;

public class PatchesModule extends AbstractModule {
    private BrewingStandFix brewingStandFix;
    private ExperienceDuplicationFix experienceDuplicationFix;
    private MuleDuplicationFix muleDuplicationFix;
    private BookGlitchesFix bookGlitchesFix;
    private MinecartDuplicationFix minecartDuplicationFix;

    public PatchesModule() {
        super("Patches");
    }

    @Override
    public ModuleStatus enable(ZCore core) {
        brewingStandFix = new BrewingStandFix();
        experienceDuplicationFix = new ExperienceDuplicationFix();
        muleDuplicationFix = new MuleDuplicationFix();
        bookGlitchesFix = new BookGlitchesFix();
        minecartDuplicationFix = new MinecartDuplicationFix();

        Bukkit.getServer().getPluginManager().registerEvents(brewingStandFix, core);
        Bukkit.getServer().getPluginManager().registerEvents(experienceDuplicationFix, core);
        Bukkit.getServer().getPluginManager().registerEvents(muleDuplicationFix, core);
        Bukkit.getServer().getPluginManager().registerEvents(bookGlitchesFix, core);
        Bukkit.getServer().getPluginManager().registerEvents(minecartDuplicationFix, core);
        return setStatus(ModuleStatus.ENABLED);
    }

    @Override
    public ModuleStatus disable(ZCore core) {
        HandlerList.unregisterAll(brewingStandFix);
        HandlerList.unregisterAll(experienceDuplicationFix);
        HandlerList.unregisterAll(muleDuplicationFix);
        HandlerList.unregisterAll(bookGlitchesFix);
        HandlerList.unregisterAll(minecartDuplicationFix);
        return setStatus(ModuleStatus.DISABLED);
    }

    @Override
    public ModuleStatus reload(ZCore core) {
        if (!disable(core).equals(ModuleStatus.DISABLED)) {
            return getStatus();
        }
        return enable(core);
    }
}
