package me.zodiakk.zcore.patches;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEnterEvent;
import org.bukkit.event.entity.EntityPortalExitEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public class MuleDuplicationFix implements Listener {
    @EventHandler
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();

        if (player.getVehicle() == null) {
            return;
        }
        player.getVehicle().remove();
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if (event.getCause().equals(PlayerTeleportEvent.TeleportCause.UNKNOWN)) {
            return;
        }

        Player player = event.getPlayer();

        if (player.getVehicle() == null) {
            return;
        }
        player.getVehicle().remove();
    }

    @EventHandler
    public void onEntityPortalEnter(EntityPortalEnterEvent event) {
        Entity entity = event.getEntity();

        if (entity.isDead()) {
            entity.remove();
        }
    }

    @EventHandler
    public void onEntityPortalExit(EntityPortalExitEvent event) {
        Entity entity = event.getEntity();

        if (entity.isDead()) {
            entity.remove();
        }
    }

    @EventHandler
    public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent event) {
        if (event.getRightClicked().getType().equals(EntityType.HORSE)) {
            Horse horse = (Horse) event.getRightClicked();

            if (horse.isCarryingChest()) {
                horse.remove();
                event.getPlayer().sendMessage("§cPour des raisons de sécurité, les ânes et mules sont désactivés.");
            }
        }
    }
}
