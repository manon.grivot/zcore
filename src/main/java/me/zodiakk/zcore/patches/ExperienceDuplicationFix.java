package me.zodiakk.zcore.patches;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEnterEvent;
import org.bukkit.event.entity.EntityPortalEvent;

public class ExperienceDuplicationFix implements Listener {
    @EventHandler
    public void onEntityPortalEnter(EntityPortalEnterEvent event) {
        if (event.getEntityType().equals(EntityType.EXPERIENCE_ORB) || event.getEntityType().equals(EntityType.THROWN_EXP_BOTTLE)) {
            event.getEntity().getWorld().getEntities().remove(event.getEntity());
        }
    }

    @EventHandler
    public void onEntityPortal(EntityPortalEvent event) {
        if (event.getEntityType().equals(EntityType.EXPERIENCE_ORB) || event.getEntityType().equals(EntityType.THROWN_EXP_BOTTLE)) {
            event.getEntity().getWorld().getEntities().remove(event.getEntity());
            event.setCancelled(true);
        }
    }
}
