package me.zodiakk.zcore.patches;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEnterEvent;
import org.bukkit.event.entity.EntityPortalExitEvent;

public class MinecartDuplicationFix implements Listener {
    @EventHandler
    public void onEntityPortal(EntityPortalEnterEvent event) {
        if (event.getEntity().getType().equals(EntityType.MINECART_CHEST)) {
            event.getEntity().remove();
        }
    }

    @EventHandler
    public void onEntityPortal(EntityPortalExitEvent event) {
        if (event.getEntity().getType().equals(EntityType.MINECART_CHEST)) {
            event.getEntity().remove();
        }
    }
}
