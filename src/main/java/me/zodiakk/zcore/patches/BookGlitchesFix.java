package me.zodiakk.zcore.patches;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerEditBookEvent;

public class BookGlitchesFix implements Listener {
    @EventHandler
    public void onPlayerEditBook(PlayerEditBookEvent event) {
        event.setCancelled(true);
        event.getPlayer().sendMessage("§cPour des raisons de sécurité, les livres sont désactivés.");
        event.getPlayer().getInventory().getItem(event.getSlot()).setAmount(0);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getCurrentItem().getType().equals(Material.BOOK_AND_QUILL) || event.getCurrentItem().getType().equals(Material.WRITTEN_BOOK)) {
            event.getWhoClicked().sendMessage("§cPour des raisons de sécurité, les livres sont désactivés.");
            event.getCurrentItem().setAmount(0);
        }
    }
}
