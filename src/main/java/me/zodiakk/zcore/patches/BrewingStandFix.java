package me.zodiakk.zcore.patches;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.inventory.ItemStack;

public class BrewingStandFix implements Listener {
    @EventHandler
    public void onBrew(BrewEvent ev) {
        ItemStack[] brewStacks = ev.getContents().getContents();
        int length = brewStacks.length;

        for (int i = 0; i < length;) {
            ItemStack stack = brewStacks[i];

            try {
                if (stack != null) {
                    // Do nothing
                }
                i++;
            } catch (NullPointerException ex) {
                if ((stack.getType().equals(Material.POTION) && (stack.getAmount() > 1))) {
                    stack.setAmount(1);
                }
            }
        }
    }
}
