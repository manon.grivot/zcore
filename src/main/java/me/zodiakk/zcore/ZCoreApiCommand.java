package me.zodiakk.zcore;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.CommandSender;

import me.zodiakk.zapi.commands.ApiCommand;
import me.zodiakk.zapi.util.ChatSession;
import me.zodiakk.zcore.modules.AbstractModule;

public final class ZCoreApiCommand implements ApiCommand {

    @Override
    public Map<String, String> getHelpLines() {
        HashMap<String, String> help = new HashMap<String, String>();

        help.put("enable <name>", "Enable a module.");
        help.put("disable <name>", "Disable a module.");
        help.put("reload <name>", "Reload a module.");
        help.put("force-enable <name>", "Force-enable a module. This command may have destructive behaviors.");
        help.put("force-reload <name>", "Force-reload a module. This command may have destructive behaviors.");
        help.put("status <name>", "Get the status of a module.");
        help.put("perror <name>", "Print the last error of a module.");
        help.put("status-all", "Get the status of all modules.");
        return help;
    }

    @Override
    public boolean onCommand(CommandSender sender, ChatSession cs, String label, String[] arg) {
        if (arg.length == 0) {
            return false;
        }
        if (arg.length == 1 && !arg[0].equalsIgnoreCase("status-all")) {
            return false;
        }

        switch (arg[0]) {
        case "enable":
            enable(cs, arg[1]);
            break;
        case "disable":
            disable(cs, arg[1]);
            break;
        case "reload":
            reload(cs, arg[1]);
            break;
        case "force-enable":
            forceEnable(cs, arg[1]);
            break;
        case "force-reload":
            forceReload(cs, arg[1]);
            break;
        case "status":
            status(cs, arg[1]);
            break;
        case "perror":
            perror(cs, arg[1]);
            break;
        case "status-all":
            statusAll(cs);
            break;
        default:
            return false;
        }
        return true;
    }

    public void enable(ChatSession cs, String module) {
        cs.send("Enabling " + module + "module...");
        ZCore.getInstance().getModuleLoader().enableModule(module);
        cs.send("New status: " + ZCore.getInstance().getModuleLoader().getModuleStatus(module).getMessage());
    }

    public void disable(ChatSession cs, String module) {
        cs.send("Disabling " + module + "module...");
        ZCore.getInstance().getModuleLoader().disableModule(module);
        cs.send("New status: " + ZCore.getInstance().getModuleLoader().getModuleStatus(module).getMessage());
    }

    public void reload(ChatSession cs, String module) {
        cs.send("Reloading " + module + "module...");
        ZCore.getInstance().getModuleLoader().reloadModule(module);
        cs.send("New status: " + ZCore.getInstance().getModuleLoader().getModuleStatus(module).getMessage());
    }

    public void forceEnable(ChatSession cs, String module) {
        cs.send("Force-enabling " + module + "module...");
        ZCore.getInstance().getModuleLoader().forceEnableModule(module);
        cs.send("New status: " + ZCore.getInstance().getModuleLoader().getModuleStatus(module).getMessage());
    }

    public void forceReload(ChatSession cs, String module) {
        cs.send("Force-reloading " + module + "module...");
        ZCore.getInstance().getModuleLoader().enableModule(module);
        cs.send("New status: " + ZCore.getInstance().getModuleLoader().getModuleStatus(module).getMessage());
    }

    public void status(ChatSession cs, String module) {
        cs.send(module + " module status: " + ZCore.getInstance().getModuleLoader().getModuleStatus(module).getMessage());
    }

    public void perror(ChatSession cs, String module) {
        cs.send(module + " module last error: " + ZCore.getInstance().getModuleLoader().getModuleLastError(module));
    }

    public void statusAll(ChatSession cs) {
        for (AbstractModule module : ZCore.getInstance().getModuleLoader().getAllModules()) {
            cs.send(module
                .getName() + ": " + module
                                        .getStatus()
                                        .getMessage());
        }
    }
}
