package me.zodiakk.zcore;

import java.util.Arrays;
import java.util.HashSet;

import org.bukkit.plugin.java.JavaPlugin;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zcore.base.BaseModule;
import me.zodiakk.zcore.coinflip.CoinflipModule;
import me.zodiakk.zcore.dailyrewards.DailyRewardsModule;
import me.zodiakk.zcore.eggdrops.EggdropsModule;
import me.zodiakk.zcore.items.CustomItemsModule;
import me.zodiakk.zcore.modules.ModuleContainer;
import me.zodiakk.zcore.modules.ModuleLoader;
import me.zodiakk.zcore.patches.PatchesModule;
import me.zodiakk.zcore.rankup.RankupModule;
import me.zodiakk.zcore.utility.UtilityModule;

public class ZCore extends JavaPlugin {
    private static ZCore instance = null;
    private ZCoreConfig config;
    private final ModuleLoader moduleLoader;

    public ZCore() {
        if (instance == null) {
            instance = this;
        }

        getDataFolder().mkdirs();
        moduleLoader = new ModuleLoader(this);
        config = new ZCoreConfig(getDataFolder());
    }

    public static final ZCore getInstance() {
        return instance;
    }

    @Override
    public void onLoad() {
        Arrays.asList("reward_grid.json", "config.json")
                .forEach(file -> config.extractFile(file));

        config.importConfigFrom("config.json", this);
    }

    @Override
    public void onEnable() {
        // Create API "modules" command
        ZApi.registerApiCommand("modules", new ZCoreApiCommand());

        // Enable modules
        // Always create base module first
        moduleLoader.loadModule(BaseModule.class, new BaseModule(), "Base");

        // Other modules
        moduleLoader.loadModule(UtilityModule.class, new UtilityModule(), "Utility");
        moduleLoader.loadModule(DailyRewardsModule.class, new DailyRewardsModule(), "DailyRewards");
        moduleLoader.loadModule(CustomItemsModule.class, new CustomItemsModule(), "CustomItems");
        moduleLoader.loadModule(CoinflipModule.class, new CoinflipModule(), "Coinflip");
        moduleLoader.loadModule(RankupModule.class, new RankupModule(), "Rankup");
        moduleLoader.loadModule(EggdropsModule.class, new EggdropsModule(), "Eggdrops");
        moduleLoader.loadModule(PatchesModule.class, new PatchesModule(), "Patches");
    }

    @Override
    public void onDisable() {
        HashSet<ModuleContainer<?>> modules = moduleLoader.getAllModuleContainers();

        // Disable enabled modules in reverse order they were enabled
        while (!modules.isEmpty()) {
            ModuleContainer<?> lastEnabled = null;

            for (ModuleContainer<?> mod : modules) {
                if (lastEnabled == null || lastEnabled.getOrder() < mod.getOrder()) {
                    lastEnabled = mod;
                }
            }

            moduleLoader.disableModule(lastEnabled);
            modules.remove(lastEnabled);
        }
    }

    public ModuleLoader getModuleLoader() {
        return moduleLoader;
    }
}
