package me.zodiakk.zcore.rankup;

import org.bukkit.command.CommandExecutor;

import me.zodiakk.zcore.ZCore;
import me.zodiakk.zcore.modules.AbstractModule;
import me.zodiakk.zcore.modules.ModuleStatus;

public class RankupModule extends AbstractModule {

    private CommandExecutor rankup;

    public RankupModule() {
        super("Rankup");
    }

    @Override
    public ModuleStatus enable(ZCore core) {
        this.rankup = new RankupCommand(getConfig().getString("track"), getConfig().getSectionArray("infos"));
        core.getCommand("rankup").setExecutor(this.rankup);
        return setStatus(ModuleStatus.ENABLED);
    }

    @Override
    public ModuleStatus disable(ZCore core) {
        core.getCommand("rankup").setExecutor(null);
        return setStatus(ModuleStatus.DISABLED);
    }

    @Override
    public ModuleStatus reload(ZCore core) {
        return (disable(core) != ModuleStatus.DISABLED) ? getStatus() : enable(core);
    }
}
