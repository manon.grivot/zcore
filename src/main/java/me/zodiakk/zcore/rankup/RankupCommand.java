package me.zodiakk.zcore.rankup;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import me.zodiakk.zapi.commands.HelpMenu;
import me.zodiakk.zapi.config.JsonConfigurationSection;
import me.zodiakk.zapi.util.ChatSession;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.context.ImmutableContextSet;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;
import net.luckperms.api.track.Track;
import net.milkbowl.vault.economy.Economy;

public class RankupCommand implements CommandExecutor {

    private static Economy economy = null;
    private static LuckPerms luckPerms = null;
    private HashSet<RankupInfos> requests;
    private Track track;
    private List<JsonConfigurationSection> infos;
    private HelpMenu help;

    public RankupCommand(String track, List<JsonConfigurationSection> infos) {
        this.requests = new HashSet<RankupInfos>();
        this.track = getLuckPerms().getTrackManager().getTrack(track);
        this.infos = infos;
        this.help = new HelpMenu("Rankup", "rankup");
        this.help.addCommand("", "Monter en grade");
        this.help.addCommand("confirm", "Confirmer une demande de rankup");
        this.help.buildMenu(6);
    }

    @Override
    public boolean onCommand(CommandSender sdr, Command command, String label, String[] args) {
        if (!(sdr instanceof Player)) {
            return true;
        }
        if (args.length > 1) {
            this.help.sendPage(sdr, 1);
            return true;
        }
        Player sender = (Player) sdr;
        ChatSession senderChat = new ChatSession("Rankup", sender.getUniqueId());
        RankupInfos request = getRankupInfos(sender);
        User user = getLuckPerms().getUserManager().getUser(sender.getUniqueId());
        double price;
        double balance;

        if (args.length == 1) {
            if (request == null) {
                senderChat.send("§cVeuillez utiliser \"/rankup\" pour monter en rang.");
                return true;
            }
            price = request.getPrice();
            balance = getEconomy().getBalance(sender);
            if (balance < price) {
                senderChat.send("§cVous n'avez pas assez d'argent pour monter en rang ! Il vous manque " + BigDecimal.valueOf(price - balance).toPlainString() + "$ pour continuer.");
                return true;
            }
            getEconomy().withdrawPlayer(sender, price);
            this.track.promote(user, ImmutableContextSet.empty());
            senderChat.send("§aVous avez été promu au rang §e" + request.getDisplay() + " §apour un montant de §e" + BigDecimal.valueOf(price).toPlainString() + "$ §a!");
            getLuckPerms().getUserManager().saveUser(user);
            removeRankupInfos(request);
            return true;
        }
        if (request != null) {
            senderChat.send("§3Vous allez être promu au rang §e" + request.getDisplay() + " §3pour un montant de §e" + BigDecimal.valueOf(request.getPrice()).toPlainString() + "$§3.");
            senderChat.send("§3Veuillez effectuer \"/rankup confirm\" pour confirmer votre action.");
            return true;
        }
        String nextGroup = this.track.getNext(getLuckPerms().getGroupManager().getGroup(getGroup(user)));

        if (nextGroup == null) {
            senderChat.send("§cVous avez déjà atteint le plus haut rang !");
            return true;
        }
        request = buildRankupInfos(nextGroup, sender);
        price = request.getPrice();
        balance = getEconomy().getBalance(sender);
        if (balance < price) {
            senderChat.send("§cVous n'avez pas assez d'argent pour monter en rang ! Il vous manque " + BigDecimal.valueOf(price - balance).toPlainString() + "$ pour continuer.");
            return true;
        }
        addRankupInfos(request);
        senderChat.send("§3Vous allez être promu au rang §e" + request.getDisplay() + " §3pour un montant de §e" + BigDecimal.valueOf(request.getPrice()).toPlainString() + "$§3.");
        senderChat.send("§3Veuillez effectuer \"/rankup confirm\" pour confirmer votre action.");
        return true;
    }

    private Economy getEconomy() {
        if (economy != null) {
            return economy;
        }
        RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);

        if (rsp == null) {
            return null;
        }
        return economy = rsp.getProvider();
    }

    private String getGroup(User user) {
        List<String> groups = this.track.getGroups();

        for (String group : groups) {
            Node node = user.getNodes().stream().filter(element -> element.getKey().equals("group." + group)).findFirst().orElse(null);

            if (node != null && node.getValue()) {
                return group;
            }
        }
        return null;
    }

    private LuckPerms getLuckPerms() {
        if (luckPerms != null) {
            return luckPerms;
        }
        RegisteredServiceProvider<LuckPerms> rsp = Bukkit.getServer().getServicesManager().getRegistration(LuckPerms.class);

        if (rsp == null) {
            return null;
        }
        return luckPerms = rsp.getProvider();
    }

    private RankupInfos getRankupInfos(Player sender) {
        for (RankupInfos infos : requests) {
            if (infos.getSender().getUniqueId().equals(sender.getUniqueId())) {
                return infos;
            }
        }
        return null;
    }

    private void addRankupInfos(RankupInfos ri) {
        requests.add(ri);
    }

    private void removeRankupInfos(RankupInfos ri) {
        for (RankupInfos infos : requests) {
            if (ri.equals(infos)) {
                requests.remove(infos);
                return;
            }
        }
    }

    private RankupInfos buildRankupInfos(String name, Player sender) {
        for (JsonConfigurationSection group : this.infos) {
            if (group.getString("name").equals(name)) {
                return new RankupInfos(sender, group.getDouble("price"), group.getString("display"));
            }
        }
        return null;
    }
}
