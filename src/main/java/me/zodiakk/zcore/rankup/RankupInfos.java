package me.zodiakk.zcore.rankup;

import org.bukkit.entity.Player;

public class RankupInfos {

    private Player sender;
    private double price;
    private String display;

    public RankupInfos(Player sender, double price, String display) {
        this.sender = sender;
        this.price = price;
        this.display = display;
    }

    public Player getSender() {
        return this.sender;
    }

    public double getPrice() {
        return this.price;
    }

    public String getDisplay() {
        return this.display;
    }
}
