package me.zodiakk.zcore.utility;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.util.ChatSession;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.cacheddata.CachedPermissionData;
import net.luckperms.api.context.ImmutableContextSet;
import net.luckperms.api.model.data.DataMutateResult;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.types.PermissionNode;
import net.luckperms.api.query.QueryOptions;
import net.milkbowl.vault.economy.Economy;

public class AutoRefundCommand implements CommandExecutor {

    private static Economy economy = null;
    private static LuckPerms luckPerms = null;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof ConsoleCommandSender) || args.length != 3) {
            return true;
        }
        Player player = Bukkit.getPlayer(args[0]);
        User user = getLuckPerms().getUserManager().getUser(player.getUniqueId());
        ChatSession chat = new ChatSession("Shop", player.getUniqueId());

        if (hasPermission(user, args[1])) {
            try {
                double price = Double.parseDouble(args[2]);

                getEconomy().depositPlayer(player, price);
                chat.send("§cVous possédiez déjà ceci, vous avez par conséquent été remboursé.");
            } catch (NumberFormatException  nfe) {
                Bukkit.getLogger().severe("Erreur : le nombre n'est pas dans le bon format.");
            }
            return true;
        }
        DataMutateResult result = user.data().add(PermissionNode.builder(args[1]).build());

        if (result != DataMutateResult.SUCCESS) {
            ZApi.postError(new IllegalStateException(result.toString()), player.getUniqueId());
            return true;
        }
        getLuckPerms().getUserManager().saveUser(user);
        return true;
    }

    private Economy getEconomy() {
        if (economy != null) {
            return economy;
        }
        RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);

        if (rsp == null) {
            return null;
        }
        return economy = rsp.getProvider();
    }

    private LuckPerms getLuckPerms() {
        if (luckPerms != null) {
            return luckPerms;
        }
        RegisteredServiceProvider<LuckPerms> rsp = Bukkit.getServer().getServicesManager().getRegistration(LuckPerms.class);

        if (rsp == null) {
            return null;
        }
        return luckPerms = rsp.getProvider();
    }

    private boolean hasPermission(User user, String permission) {
        ImmutableContextSet contextSet = getLuckPerms().getContextManager().getContext(user).orElseGet(getLuckPerms().getContextManager()::getStaticContext);
        CachedPermissionData permissionData = user.getCachedData().getPermissionData(QueryOptions.contextual(contextSet));

        return permissionData.checkPermission(permission).asBoolean();
    }
}

