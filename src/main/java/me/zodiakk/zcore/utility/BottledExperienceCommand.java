package me.zodiakk.zcore.utility;

import java.util.Map;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.zodiakk.zapi.util.ChatSession;

public class BottledExperienceCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player) || args.length != 0) {
            return true;
        }
        Player player = (Player) sender;
        ChatSession chat = new ChatSession("Experience", player.getUniqueId());
        int exp = getExp(player);
        int bottles = exp / 14;

        if (bottles == 0) {
            chat.send("§cVous n'avez pas assez d'expérience pour la transformer en bouteille(s).");
            return true;
        }
        changeExp(player, -(exp - (exp % 14)));
        chat.send("§7Vous avez transformé §e" + (exp - (exp % 14)) + " §7points d'expérience en §e" + bottles + " §7bouteille(s).");
        giveExpToPlayer(bottles, player);
        return true;
    }

    private int getExp(Player player) {
        return getExpFromLevel(player.getLevel()) + Math.round(getExpToNext(player.getLevel()) * player.getExp());
    }

    private int getExpFromLevel(int level) {
        if (level > 30) {
            return (int) (4.5 * level * level - 162.5 * level + 2220);
        }
        if (level > 15) {
            return (int) (2.5 * level * level - 40.5 * level + 360);
        }
        return level * level + 6 * level;
    }

    private double getLevelFromExp(long exp) {
        if (exp > 1395) {
            return (Math.sqrt(72 * exp - 54215) + 325) / 18;
        }
        if (exp > 315) {
            return Math.sqrt(40 * exp - 7839) / 10 + 8.1;
        }
        if (exp > 0) {
            return Math.sqrt(exp + 9) - 3;
        }
        return 0;
    }

    private int getExpToNext(int level) {
        if (level > 30) {
            return 9 * level - 158;
        }
        if (level > 15) {
            return 5 * level - 38;
        }
        return 2 * level + 7;
    }

    private void changeExp(Player player, int exp) {
        exp += getExp(player);
        if (exp < 0) {
            exp = 0;
        }
        double levelAndExp = getLevelFromExp(exp);
        int level = (int) levelAndExp;

        player.setLevel(level);
        player.setExp((float) (levelAndExp - level));
    }

    private void giveExpToPlayer(int amount, Player player) {
        ItemStack[] stacks = new ItemStack[amount / 64 + 1];

        for (int i = 0; i < stacks.length; i++) {
            stacks[i] = new ItemStack(Material.EXP_BOTTLE, amount > 64 ? 64 : amount);
            amount -= 64;
        }
        giveItemsToPlayer(stacks, player);
    }

    private void giveItemsToPlayer(ItemStack[] items, Player player) {
        Map<Integer, ItemStack> remaining = player.getInventory().addItem(items);

        if (!remaining.isEmpty()) {
            for (ItemStack stack : remaining.values()) {
                player.getLocation().getWorld().dropItemNaturally(player.getLocation(), stack);
            }
        }
    }
}
