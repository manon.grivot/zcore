package me.zodiakk.zcore.utility;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;

import me.zodiakk.zapi.util.ItemUtil;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.cacheddata.CachedPermissionData;
import net.luckperms.api.context.ImmutableContextSet;
import net.luckperms.api.model.user.User;
import net.luckperms.api.query.QueryOptions;

public class ColoredAnvilsListener implements Listener {

    private static LuckPerms luckPerms = null;

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getInventory().getType() != InventoryType.ANVIL || event.getSlotType() != SlotType.RESULT || event.getCurrentItem() == null
                || (event.getClick() != ClickType.LEFT && event.getClick() != ClickType.SHIFT_LEFT)
                || !hasPermission(getLuckPerms().getUserManager().getUser(event.getWhoClicked().getUniqueId()), "zcore.colored-anvil")) {
            return;
        }
        ItemStack item = event.getCurrentItem();
        ItemMeta meta = ItemUtil.getItemMeta(item);
        String name = meta.getDisplayName();

        name = ChatColor.translateAlternateColorCodes('&', name);
        meta.setDisplayName(name);
        item.setItemMeta(meta);
    }

    private LuckPerms getLuckPerms() {
        if (luckPerms != null) {
            return luckPerms;
        }
        RegisteredServiceProvider<LuckPerms> rsp = Bukkit.getServer().getServicesManager().getRegistration(LuckPerms.class);

        if (rsp == null) {
            return null;
        }
        return luckPerms = rsp.getProvider();
    }

    private boolean hasPermission(User user, String permission) {
        ImmutableContextSet contextSet = getLuckPerms().getContextManager().getContext(user).orElseGet(getLuckPerms().getContextManager()::getStaticContext);
        CachedPermissionData permissionData = user.getCachedData().getPermissionData(QueryOptions.contextual(contextSet));

        return permissionData.checkPermission(permission).asBoolean();
    }
}
