package me.zodiakk.zcore.utility;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.event.HandlerList;

import me.zodiakk.zcore.ZCore;
import me.zodiakk.zcore.modules.AbstractModule;
import me.zodiakk.zcore.modules.ModuleStatus;

public class UtilityModule extends AbstractModule {

    private AntiDisconnectSpamListener antiDisconnectSpamListener;
    private CommandExecutor arperm;
    private CommandExecutor bottleexp;
    private ColoredAnvilsListener coloredAnvilsListener;
    private CommandExecutor trash;

    public UtilityModule() {
        super("Utility");
    }

    @Override
    public ModuleStatus enable(ZCore core) {
        this.antiDisconnectSpamListener = new AntiDisconnectSpamListener();
        Bukkit.getServer().getPluginManager().registerEvents(this.antiDisconnectSpamListener, core);
        this.arperm = new AutoRefundCommand();
        core.getCommand("arperm").setExecutor(this.arperm);
        this.bottleexp = new BottledExperienceCommand();
        core.getCommand("bottleexp").setExecutor(this.bottleexp);
        this.coloredAnvilsListener = new ColoredAnvilsListener();
        Bukkit.getServer().getPluginManager().registerEvents(this.coloredAnvilsListener, core);
        this.trash = new TrashCommand();
        core.getCommand("trash").setExecutor(this.trash);
        return setStatus(ModuleStatus.ENABLED);
    }

    @Override
    public ModuleStatus disable(ZCore core) {
        HandlerList.unregisterAll(this.antiDisconnectSpamListener);
        core.getCommand("arperm").setExecutor(null);
        core.getCommand("bottleexp").setExecutor(null);
        HandlerList.unregisterAll(this.coloredAnvilsListener);
        core.getCommand("trash").setExecutor(null);
        return setStatus(ModuleStatus.DISABLED);
    }

    @Override
    public ModuleStatus reload(ZCore core) {
        return (disable(core) != ModuleStatus.DISABLED) ? getStatus() : enable(core);
    }
}
