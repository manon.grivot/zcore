package me.zodiakk.zcore.utility;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;

public class AntiDisconnectSpamListener implements Listener {

    @EventHandler
    public void onPlayerKick(PlayerKickEvent event) {
        if (event.getReason().equals("disconnect.spam")) {
            event.setCancelled(true);
        }
    }
}
