package me.zodiakk.zcore.dailyrewards;

import java.io.File;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.scheduler.BukkitTask;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.config.JsonConfigurationSection;
import me.zodiakk.zapi.data.result.ResultRow;
import me.zodiakk.zapi.data.table.TableField;
import me.zodiakk.zapi.data.table.TableField.DefaultValue;
import me.zodiakk.zapi.data.table.TableField.FieldType;
import me.zodiakk.zapi.data.table.TableField.IndexType;
import me.zodiakk.zapi.data.table.TableModel;
import me.zodiakk.zapi.data.table.TableRow;
import me.zodiakk.zapi.util.ChatSession;
import me.zodiakk.zapi.util.JsonUtil;
import me.zodiakk.zcore.ZCore;
import me.zodiakk.zcore.ZCoreConfig;
import me.zodiakk.zcore.ZCoreLogger;
import me.zodiakk.zcore.dailyrewards.rewards.RewardGrid;
import me.zodiakk.zcore.modules.AbstractModule;
import me.zodiakk.zcore.modules.ModuleStatus;

public class DailyRewardsModule extends AbstractModule {
    private static final int CURRENT_DAY;

    private HashMap<UUID, Integer> timeLeft;
    private HashMap<UUID, DailyRewardsProfile> profileCache;
    private HashSet<UUID> joinedOnce;
    private TableModel table;
    private int rewardTime;
    private DailyRewardsListener listener;
    private DailyRewardsTask task;
    private DailyRewardsCommand command;
    private BukkitTask runningTask;
    private RewardGrid rewardGrid;

    static {
        CURRENT_DAY = (int) (ChronoUnit.DAYS.between(LocalDate.ofEpochDay(0L), LocalDate.now()));
    }

    public DailyRewardsModule() {
        super("DailyRewards");

        this.timeLeft = new HashMap<UUID, Integer>();
        this.joinedOnce = new HashSet<UUID>();
        this.profileCache = new HashMap<UUID, DailyRewardsProfile>();
        this.listener = new DailyRewardsListener(this);
        this.task = new DailyRewardsTask(this);
        this.command = new DailyRewardsCommand();
    }

    @Override
    public ModuleStatus enable(ZCore core) {
        JsonConfigurationSection cfg = ZCoreConfig.getInstance().getModuleConfig("DailyRewards");
        File gridFile = new File(core.getDataFolder(), cfg.getString("gridFile"));

        if (!gridFile.exists() || !gridFile.isFile()) {
            ZCoreLogger.error("DailyRewards", "Grid file doesn't exists.");
            return setStatus(ModuleStatus.FAILED_TO_LOAD);
        }

        this.rewardGrid = new RewardGrid(JsonUtil.readJsonFile(gridFile).getAsJsonArray());

        try {
            TableModel stats = ZApi.getData().getTableManager().getModel("daily_rewards");

            if (stats == null) {
                stats = new TableModel("daily_rewards");
                ZApi.getData().getTableManager().setModel(stats);
            }

            stats.addField("uuid", new TableField<String>("uuid", FieldType.VARCHAR, 36, IndexType.PRIMARY));
            stats.addField("last_day", new TableField<Integer>("last_day", FieldType.INT, new DefaultValue<Integer>(Integer.valueOf(0))));
            stats.addField("last_id", new TableField<Integer>("last_id", FieldType.INT, new DefaultValue<Integer>(0)));
            stats.addField("not_rewarded", new TableField<Integer>("not_rewarded", FieldType.INT, new DefaultValue<Integer>(0)));
            stats.addField("stats_actual_consecutive", new TableField<Integer>("stats_actual_consecutive", FieldType.INT, new DefaultValue<Integer>(0)));
            stats.addField("stats_max_consecutive", new TableField<Integer>("stats_max_consecutive", FieldType.INT, new DefaultValue<Integer>(0)));
            stats.addField("stats_total_received", new TableField<Integer>("stats_total_received", FieldType.INT, new DefaultValue<Integer>(0)));
            table = stats;
        } catch (Exception ex) {
            ZApi.postError(ex);
            return setStatus(ModuleStatus.FAILED_TO_LOAD);
        }
        rewardTime = cfg.getInteger("rewardTime");
        runningTask = ZApi.getThreadsManager().executeTimer(task, 20L, 20L);
        Bukkit.getServer().getPluginManager().registerEvents(listener, core);
        core.getCommand("dailyrewards").setExecutor(command);
        return setStatus(ModuleStatus.ENABLED);
    }

    @Override
    public ModuleStatus disable(ZCore core) {
        runningTask.cancel();
        HandlerList.unregisterAll(listener);
        core.getCommand("dailyrewards").setExecutor(null);
        return setStatus(ModuleStatus.DISABLED);
    }

    @Override
    public ModuleStatus reload(ZCore core) {
        if (!disable(core).equals(ModuleStatus.DISABLED)) {
            return getStatus();
        }
        return enable(core);
    }

    private void reward(UUID uPlayer) {
        ChatSession cs = new ChatSession("Récompenses journalières", uPlayer);

        try {
            Player player = Bukkit.getPlayer(uPlayer);
            DailyRewardsProfile profile = getPlayerProfile(player.getUniqueId());

            profile.updateValues(CURRENT_DAY);
            profile.updateDatabase();
            cs.send("&aVotre récompense journalière est disponible ! Faites \"/daily\" pour la récupérer.");
        } catch (SQLException ex) {
            ZApi.postError(ex, uPlayer);
        }
    }

    public void addPlayer(UUID player, DailyRewardsProfile profile) {
        if (!profile.isTodayReceived()) {
            synchronized (timeLeft) {
                timeLeft.put(player, rewardTime);
            }
        }

        synchronized (joinedOnce) {
            joinedOnce.add(player);
        }
    }

    public void announcePlayer(UUID player) {
        Integer time;
        DailyRewardsProfile profile = getPlayerProfile(player);

        if (profile.isTodayReceived()) {
            return;
        }

        synchronized (timeLeft) {
            time = timeLeft.get(player);
        }
        if (time == null) {
            return;
        }
        ChatSession.sendOne(player, "Récompenses journalières", "&3Temps restant avant de reçevoir votre récompense journalières : &e" + getTimeString(time) + "&3.");
    }

    public Integer getPlayerRemainingTime(UUID player) {
        synchronized (timeLeft) {
            return timeLeft.get(player);
        }
    }

    public boolean hasJoinedOnce(UUID player) {
        synchronized (joinedOnce) {
            if (joinedOnce.contains(player)) {
                return true;
            }
        }
        return false;
    }

    public void countDown() {
        HashSet<UUID> onlinePlayers = new HashSet<UUID>();

        synchronized (timeLeft) {

        }
        for (Player player : Bukkit.getOnlinePlayers()) {
            onlinePlayers.add(player.getUniqueId());
        }
        synchronized (timeLeft) {
            for (Entry<UUID, Integer> entry : timeLeft.entrySet()) {
                if (!onlinePlayers.contains(entry.getKey())) {
                    continue;
                }
                entry.setValue(entry.getValue() - 1);

                if (entry.getValue() == 0) {
                    reward(entry.getKey());
                    timeLeft.remove(entry.getKey());
                }
            }
        }
    }

    public RewardGrid getRewardGrid() {
        return rewardGrid;
    }

    public void setPlayerProfile(UUID player, DailyRewardsProfile profile) {
        synchronized (profileCache) {
            profileCache.put(player, profile);
        }
    }

    public DailyRewardsProfile getPlayerProfile(UUID player) {
        synchronized (profileCache) {
            return profileCache.get(player);
        }
    }

    public void removePlayerProfile(UUID player) {
        synchronized (profileCache) {
            profileCache.remove(player);
        }
    }

    public DailyRewardsProfile retrievePlayerProfile(UUID player) {
        try {
            ResultRow resultRow = table.selectOne(player.toString());

            if (resultRow == null) {
                TableRow newRow = new TableRow(table);

                newRow.setValue("uuid", player.toString());
                table.insert(newRow);
                return retrievePlayerProfile(player);
            }
            return new DailyRewardsProfile(resultRow);
        } catch (SQLException ex) {
            ZCoreLogger.error("DailyRewards", ex);
            ZApi.getThreadsManager().executeInMainThread(new Runnable() {
                @Override
                public void run() {
                    Bukkit.getPlayer(player).kickPlayer("§cUne erreur est survenue.\n§cSi le problème persiste, merci de contacter un administrateur.");
                }
            });
            return null;
        }
    }

    public static String getTimeString(Integer time) {
        if (time == null || time <= 0) {
            return "§aObtenu !";
        }
        return "" + (time < 600 ? "0" : "") + (time / 60) + ":" + (time % 60 < 10 ? "0" : "") + (time % 60);
    }

    public static int getCurrentDay() {
        return CURRENT_DAY;
    }
}
