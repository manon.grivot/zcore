package me.zodiakk.zcore.dailyrewards;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.zodiakk.zapi.ZApi;

public class DailyRewardsListener implements Listener {
    DailyRewardsModule module;

    public DailyRewardsListener(DailyRewardsModule module) {
        this.module = module;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        ZApi.getThreadsManager().execute(new Runnable() {
            @Override
            public void run() {
                Player player = event.getPlayer();

                if (!module.hasJoinedOnce(player.getUniqueId())) {
                    DailyRewardsProfile profile = module.retrievePlayerProfile(player.getUniqueId());

                    module.setPlayerProfile(player.getUniqueId(), profile);
                    module.addPlayer(player.getUniqueId(), profile);
                }
                module.announcePlayer(player.getUniqueId());
            }
        });
    }
}
