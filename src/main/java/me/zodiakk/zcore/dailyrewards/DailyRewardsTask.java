package me.zodiakk.zcore.dailyrewards;

import me.zodiakk.zapi.ZApi;

public class DailyRewardsTask implements Runnable {
    private DailyRewardsModule module;

    public DailyRewardsTask(DailyRewardsModule module) {
        this.module = module;
    }

    @Override
    public void run() {
        ZApi.getThreadsManager().execute(new Runnable() {
            @Override
            public void run() {
                module.countDown();
            }
        });
    }
}
