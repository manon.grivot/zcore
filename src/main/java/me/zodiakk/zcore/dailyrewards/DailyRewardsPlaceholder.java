package me.zodiakk.zcore.dailyrewards;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;

public class DailyRewardsPlaceholder extends PlaceholderExpansion {
    private Plugin plugin;
    private DailyRewardsModule module;

    public DailyRewardsPlaceholder(Plugin plugin, DailyRewardsModule module) {
        this.plugin = plugin;
        this.module = module;
    }

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getAuthor() {
        return "Zodiak";
    }

    @Override
    public String getIdentifier() {
        return "zcore-dailyrewards";
    }

    @Override
    public String getVersion() {
        return plugin.getDescription().getVersion();
    }

    @Override
    public String onPlaceholderRequest(Player player, String identifier) {
        if (player == null) {
            return "";
        }

        if (identifier.equals("remaining")) {
            Integer time = module.getPlayerRemainingTime(player.getUniqueId());

            if (time == null) {
                return "Reçu";
            }
            return DailyRewardsModule.getTimeString(time);
        }

        return "";
    }
}
