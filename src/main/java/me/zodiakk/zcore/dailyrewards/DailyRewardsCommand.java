package me.zodiakk.zcore.dailyrewards;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import fr.minuskube.inv.SmartInventory;
import me.zodiakk.zapi.util.ChatSession;

public class DailyRewardsCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] arg) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("§cVous devez être un joueur pour effectuer cette commande.");
            return true;
        }

        Player player = (Player) sender;

        if (arg.length != 0) {
            ChatSession.sendOne(player.getUniqueId(), "Récompenses journalières", "Utilisation : /" + label);
            return true;
        }

        openMenu(player);

        return true;
    }

    private void openMenu(Player player) {
        SmartInventory.Builder builder = SmartInventory.builder();

        SmartInventory playerInventory = builder.id("DailyRewards-" + player.getUniqueId().toString())
                                                .title("§2§lRécompenses journalières §8§l» §3§l" + player.getName())
                                                .type(InventoryType.CHEST)
                                                .size(6, 9)
                                                .closeable(true)
                                                .provider(new MenuProvider())
                                                .build();
        playerInventory.open(player);
    }
}
