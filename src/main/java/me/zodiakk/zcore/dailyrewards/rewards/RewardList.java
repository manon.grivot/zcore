package me.zodiakk.zcore.dailyrewards.rewards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.clip.placeholderapi.PlaceholderAPI;
import me.zodiakk.zapi.ZApi;
import net.milkbowl.vault.economy.Economy;

public class RewardList {
    private static final Economy ECONOMY;

    private List<String> commandRewards;
    private List<ItemStack> itemRewards;
    private Double moneyReward;

    static {
        ECONOMY = Bukkit.getServer().getServicesManager().getRegistration(Economy.class).getProvider();
    }

    public RewardList() {
        commandRewards = new ArrayList<String>();
        itemRewards = new ArrayList<ItemStack>();
        moneyReward = null;
    }

    public void setMoney(Double money) {
        moneyReward = money;
    }

    public void resetMoney() {
        moneyReward = null;
    }

    public void addCommand(String command) {
        commandRewards.add(command);
    }

    public boolean removeCommand(String command) {
        return commandRewards.remove(command);
    }

    public void resetCommands() {
        commandRewards.clear();
    }

    public void addItem(ItemStack item) {
        itemRewards.add(item);
    }

    public boolean removeItem(ItemStack item) {
        return itemRewards.remove(item);
    }

    public void resetItems() {
        itemRewards.clear();
    }

    public void rewardPlayer(Player player) {
        ZApi.getThreadsManager().execute(new Runnable() {
            @Override
            public void run() {
                // Money
                if (moneyReward != null) {
                    ECONOMY.depositPlayer(player, moneyReward);
                }

                // Items
                if (itemRewards.size() > 0) {
                    HashMap<Integer, ItemStack> remaining = player.getInventory().addItem(itemRewards.toArray(new ItemStack[itemRewards.size()]));

                    for (ItemStack remainingItem : remaining.values()) {
                        player.getLocation().getWorld().dropItemNaturally(player.getLocation(), remainingItem);
                    }
                }

                // Commands
                if (commandRewards.size() > 0) {
                    ArrayList<String> parsedCommands = new ArrayList<String>();
                    for (String command : commandRewards) {
                        parsedCommands.add(PlaceholderAPI.setPlaceholders(player, command));
                    }

                    ZApi.getThreadsManager().executeInMainThread(new Runnable() {
                        @Override
                        public void run() {
                            for (String command : parsedCommands) {
                                Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), command);
                            }
                        }
                    });
                }
            }
        });
    }
}
