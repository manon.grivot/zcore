package me.zodiakk.zcore.dailyrewards.rewards;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.zodiakk.zapi.util.JsonUtil;

public class RewardDay {
    private int day;
    private ItemStack decoration;
    private RewardList rewards;

    public RewardDay(JsonObject config, int idx) {
        this.day = idx + 1;
        this.decoration = JsonUtil.itemStackFromJson(config.get("decoration").getAsJsonObject());
        this.rewards = new RewardList();

        for (JsonElement element : config.get("rewards").getAsJsonArray()) {
            JsonObject rewardsObject = element.getAsJsonObject();

            if (rewardsObject.has("money")) {
                rewards.setMoney(rewardsObject.get("money").getAsDouble());
            }

            if (rewardsObject.has("items")) {
                for (JsonElement itemReward : rewardsObject.get("items").getAsJsonArray()) {
                    rewards.addItem(JsonUtil.itemStackFromJson(itemReward.getAsJsonObject()));
                }
            }

            if (rewardsObject.has("commands")) {
                for (JsonElement commandReward : rewardsObject.get("commands").getAsJsonArray()) {
                    rewards.addCommand(commandReward.getAsString());
                }
            }
        }
    }

    public int getDay() {
        return day;
    }

    public ItemStack getDecoration() {
        return decoration;
    }

    public void rewardPlayer(Player player) {
        rewards.rewardPlayer(player);
    }
}
