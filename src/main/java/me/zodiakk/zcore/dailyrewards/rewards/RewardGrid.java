package me.zodiakk.zcore.dailyrewards.rewards;

import java.util.HashSet;

import com.google.gson.JsonArray;

import org.bukkit.entity.Player;

public class RewardGrid {
    private HashSet<RewardDay> rewards;

    public RewardGrid(JsonArray config) {
        rewards = new HashSet<RewardDay>();

        for (int i = 0; i < config.size(); i++) {
            rewards.add(new RewardDay(config.get(i).getAsJsonObject(), i));
        }
    }

    public RewardDay getDay(int day) {
        for (RewardDay reward : rewards) {
            if (reward.getDay() == day) {
                return reward;
            }
        }
        return null;
    }

    public void rewardPlayer(Player player, int day) {
        getDay(day).rewardPlayer(player);
    }
}
