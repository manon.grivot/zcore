package me.zodiakk.zcore.dailyrewards;

import java.sql.SQLException;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.data.result.ResultRow;
import me.zodiakk.zapi.data.table.TableRow;

public class DailyRewardsProfile {
    TableRow row;
    String uuid;
    int notRewarded;
    int lastDay;
    int lastId;
    int statsActualConsecutive;
    int statsMaxConsecutive;
    int statsTotalReceived;

    public DailyRewardsProfile(ResultRow row) {
        this.row = row.toTableRow();
        uuid = (String) (row.getValue("uuid").getValue());
        notRewarded = (Integer) (row.getValue("not_rewarded").getValue());
        lastDay = (Integer) (row.getValue("last_day").getValue());
        lastId = (Integer) (row.getValue("last_id").getValue());
        statsActualConsecutive = (Integer) (row.getValue("stats_actual_consecutive").getValue());
        statsMaxConsecutive = (Integer) (row.getValue("stats_max_consecutive").getValue());
        statsTotalReceived = (Integer) (row.getValue("stats_total_received").getValue());
    }

    public void updateValues(int currentDay) {
        if (lastDay == currentDay - 1) {
            if (lastId == 28) {
                lastId = 27;
            }
            if (!isDayAvailable(lastId + 1)) {
                notRewarded += intPow(2, lastId);
            }
            lastDay = currentDay;
            lastId++;
            statsActualConsecutive++;
            if (statsActualConsecutive > statsMaxConsecutive) {
                statsMaxConsecutive = statsActualConsecutive;
            }
            statsTotalReceived++;
        } else {
            lastId = 1;
            notRewarded = 1;
            lastDay = currentDay;
            statsActualConsecutive = 1;
            if (statsMaxConsecutive == 0) {
                statsMaxConsecutive = 1;
            }
            statsTotalReceived++;
        }
    }

    public void updateDatabase() throws SQLException {
        row.setValue("last_day", lastDay);
        row.setValue("last_id", lastId);
        row.setValue("not_rewarded", notRewarded);
        row.setValue("stats_actual_consecutive", statsActualConsecutive);
        row.setValue("stats_max_consecutive", statsMaxConsecutive);
        row.setValue("stats_total_received", statsTotalReceived);
        ZApi.getData().getTableManager().getModel("daily_rewards").updateRows(row);
    }

    public boolean isDayRetrieved(int day) {
        if (lastDay + 1 < DailyRewardsModule.getCurrentDay()) {
            return false;
        }
        if (day == 28 && getTodayId() == 28 && !isTodayReceived()) {
            return false;
        }
        if (day > lastId) {
            return false;
        }
        return (intPow(2, day - 1) & notRewarded) == 0;
    }

    public boolean isDayAvailable(int day) {
        return (intPow(2, day - 1) & notRewarded) != 0;
    }

    public int getTodayId() {
        if (lastDay == DailyRewardsModule.getCurrentDay()) {
            return lastId;
        }
        if (lastDay + 1 == DailyRewardsModule.getCurrentDay()) {
            return lastId >= 28 ? 28 : lastId + 1;
        }
        return 1;
    }

    public boolean isTodayReceived() {
        return lastDay == DailyRewardsModule.getCurrentDay();
    }

    public void setDayRewarded(Integer day) throws SQLException {
        notRewarded -= intPow(2, day - 1);
        try {
            updateDatabase();
        } catch (SQLException ex) {
            notRewarded += intPow(2, day + 1);
            throw ex;
        }
    }

    private int intPow(int number, int power) {
        int value = 1;

        for (int i = 0; i < power; i++) {
            value *= number;
        }
        return value;
    }
}
