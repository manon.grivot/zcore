package me.zodiakk.zcore.dailyrewards;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.function.Consumer;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.util.ChatSession;
import me.zodiakk.zapi.util.ItemUtil;
import me.zodiakk.zcore.ZCore;

public class MenuProvider implements InventoryProvider {
    private static final int ROWS = 6;
    private static final int COLS = 9;
    private static final int DAYS = 28;
    private static final int DAYS_PER_WEEK = 7;

    private static final ClickableItem GLASS_ITEM;
    private static final ClickableItem CLOSE_ITEM;
    private static final Consumer<InventoryClickEvent> DAY_CLICK_ACTION;

    private static DailyRewardsModule module;

    static {
        ItemStack glassStack = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
        ItemMeta glassMeta = ItemUtil.getItemMeta(glassStack);
        ItemStack barrierStack = new ItemStack(Material.BARRIER, 1);
        ItemMeta barrierMeta = ItemUtil.getItemMeta(barrierStack);

        glassMeta.setDisplayName("");
        barrierMeta.setDisplayName("§cFermer");

        glassStack.setItemMeta(glassMeta);
        barrierStack.setItemMeta(barrierMeta);

        GLASS_ITEM = ClickableItem.of(glassStack, event -> {
            event.setResult(Result.DENY);
            event.setCancelled(true);
        });

        CLOSE_ITEM = ClickableItem.of(barrierStack, event -> {
            event.getWhoClicked().closeInventory();
        });

        DAY_CLICK_ACTION = event -> {
            int day = event.getSlot();
            Player player = ((Player) event.getWhoClicked());
            DailyRewardsProfile profile = module.getPlayerProfile(player.getUniqueId());
            ChatSession cs = new ChatSession("Récompenses journalières", player.getUniqueId());

            day -= ((day / COLS) * 2) + DAYS_PER_WEEK;
            if (!profile.isDayAvailable(day)) {
                if (profile.isDayRetrieved(day)) {
                    cs.send("&cVous avez déjà récupéré cette récompense.");
                } else {
                    cs.send("&cVous n'avez pas encore débloqué cette récompense.");
                }
                player.playSound(player.getLocation(), Sound.VILLAGER_NO, 100.0f, 1.0f);
            } else {
                module.getRewardGrid().rewardPlayer(player, day);
                player.playSound(player.getLocation(), Sound.ORB_PICKUP, 100.0f, 2.0f);
                try {
                    profile.setDayRewarded(day);
                } catch (SQLException ex) {
                    ZApi.postError(ex, player);
                }
            }
        };
    }

    public MenuProvider() {
        if (module == null) {
            module = ZCore.getInstance().getModuleLoader().getModule(DailyRewardsModule.class);
        }
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        DailyRewardsProfile profile = module.getPlayerProfile(player.getUniqueId());

        for (int i = 0; i < COLS; i++) {
            contents.set(0, i, GLASS_ITEM);
            contents.set(ROWS - 1, i, i == COLS / 2 ? CLOSE_ITEM : GLASS_ITEM);
            if (i > 0 && i < ROWS - 1) {
                contents.set(i, 0, GLASS_ITEM);
                contents.set(i, COLS - 1, GLASS_ITEM);
            }
        }

        for (int i = 1; i <= DAYS; i++) {
            final int row = 1 + ((i - 1) / DAYS_PER_WEEK);
            final int col = ((i - 1) % DAYS_PER_WEEK) + 1;
            ItemStack decoration = module.getRewardGrid().getDay(i).getDecoration().clone();
            decoration.setAmount(i);
            ItemMeta meta = ItemUtil.getItemMeta(decoration);

            if (!meta.hasLore()) {
                meta.setLore(new ArrayList<String>());
            }

            if (profile.isDayRetrieved(i)) {
                decoration.setAmount(-1);
            } else if (profile.isDayAvailable(i)) {
                meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            }
            if (profile.getTodayId() == i) {
                meta.getLore().add(0, "§7Temps restant : §e" + DailyRewardsModule.getTimeString(module.getPlayerRemainingTime(player.getUniqueId())));
                meta.getLore().add(1, "§r");
            }
            decoration.setItemMeta(meta);
            contents.set(row, col, ClickableItem.of(decoration, DAY_CLICK_ACTION));
        }
    }

    @Override
    public void update(Player player, InventoryContents contents) {
        DailyRewardsProfile profile = module.getPlayerProfile(player.getUniqueId());

        for (int i = 1; i <= DAYS; i++) {
            final int row = 1 + ((i - 1) / DAYS_PER_WEEK);
            final int col = ((i - 1) % DAYS_PER_WEEK) + 1;
            ItemStack decoration = contents.get(row, col).get().getItem();

            if (decoration.getAmount() == -1) {
                continue;
            }
            if (decoration.getAmount() > profile.getTodayId()) {
                continue;
            }
            ItemMeta meta = ItemUtil.getItemMeta(decoration);

            if (profile.isDayRetrieved(i)) {
                decoration.setAmount(-1);
                meta.removeEnchant(Enchantment.PROTECTION_ENVIRONMENTAL);
            }
            if (profile.getTodayId() == i) {
                meta.getLore().set(0, "§7Temps restant : §e" + DailyRewardsModule.getTimeString(module.getPlayerRemainingTime(player.getUniqueId())));
            }
            decoration.setItemMeta(meta);
            contents.set(row, col, ClickableItem.of(decoration, DAY_CLICK_ACTION));
        }
    }
}
