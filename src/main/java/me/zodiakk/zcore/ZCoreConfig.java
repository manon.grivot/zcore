package me.zodiakk.zcore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

import me.zodiakk.zapi.ZApi;
import me.zodiakk.zapi.config.JsonConfiguration;
import me.zodiakk.zapi.config.JsonConfigurationSection;

public class ZCoreConfig {
    private File workingDirectory;
    private JsonConfiguration config;
    private HashSet<String> disabledModules;
    private Boolean debug;

    private static ZCoreConfig INSTANCE = null;

    public ZCoreConfig(File workingDirectory) {
        if (INSTANCE == null) {
            INSTANCE = this;
        } else {
            return;
        }

        this.workingDirectory = workingDirectory;
    }

    public void importConfigFrom(String fileName, Plugin plugin) {
        config = JsonConfiguration.getConfiguration(new File(workingDirectory, fileName));

        config.updateConfiguration(plugin, fileName, true);
        disabledModules = new HashSet<String>();
        config.getStringArray("disabledModules").forEach(module -> disabledModules.add(module));
        debug = config.getBoolean("debug");
    }

    public JsonConfiguration readConfigFile(String fileName, boolean reload) {
        return JsonConfiguration.getConfiguration(new File(workingDirectory, fileName), reload);
    }

    public boolean isModuleDisabled(String moduleName) {
        return disabledModules.contains(moduleName);
    }

    public Collection<String> getDisabledModules() {
        return disabledModules;
    }

    public boolean getDebug() {
        return debug;
    }

    public JsonConfigurationSection getModuleConfig(String moduleName) {
        return config.getSection("modules").getSection(moduleName);
    }

    public static ZCoreConfig getInstance() {
        return INSTANCE;
    }

    public void extractFile(String inputFile) {
        File configFile = new File(workingDirectory, inputFile);

        if (!configFile.exists()) {
            extractFile(configFile, inputFile);
        }
    }

    public void extractFile(File outputFile, String inputFile) {
        try {
            outputFile.createNewFile();
            outputFile.setReadable(true);
            outputFile.setWritable(true);

            InputStream stream = getClass().getResourceAsStream("/" + inputFile);
            FileOutputStream output = new FileOutputStream(outputFile);
            byte[] buffer = new byte[1024];

            for (int i = 0; (i = stream.read(buffer)) != -1;) {
                output.write(buffer, 0, i);
            }
            output.close();
            stream.close();
        } catch (Exception ex) {
            ZApi.postError(ex);
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "ERROR: Unable to write config files.");
        }
    }
}
