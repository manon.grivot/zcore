package me.zodiakk.zcore.eggdrops;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import me.zodiakk.zapi.util.RandomUtil;

public class EggdropsListener implements Listener {

    private EggdropsConfiguration config;

    public EggdropsListener(EggdropsConfiguration config) {
        this.config = config;
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        EntityType entity = event.getEntityType();
        Player killer = event.getEntity().getKiller();

        if (entity == EntityType.PLAYER) {
            return;
        }
        int lootingLevel;

        if (killer == null) {
            lootingLevel = 0;
        } else {
            lootingLevel = killer.getInventory().getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_MOBS);
        }
        double drop = this.config.getDropPercent(entity) * (1 + lootingLevel * this.config.getLootingChance());
        boolean canDrop = RandomUtil.testChance(drop);

        if (drop == 0 || !canDrop) {
            return;
        }
        ItemStack egg = new ItemStack(Material.MONSTER_EGG, 1, this.config.getEggID(entity));

        event.getEntity().getLocation().getWorld().dropItemNaturally(event.getEntity().getLocation(), egg);
    }
}
