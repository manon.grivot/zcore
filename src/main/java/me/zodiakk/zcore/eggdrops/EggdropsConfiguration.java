package me.zodiakk.zcore.eggdrops;

import java.util.HashMap;

import org.bukkit.entity.EntityType;

import me.zodiakk.zapi.config.JsonConfigurationSection;

public class EggdropsConfiguration {

    private JsonConfigurationSection config;
    private double lootingChance;
    private HashMap<EntityType, Double> eggDropPercents;
    private HashMap<EntityType, Short> eggIDs;

    public EggdropsConfiguration(JsonConfigurationSection config) {
        this.config = config;
        this.eggDropPercents = new HashMap<EntityType, Double>();
        this.eggIDs.put(EntityType.COW, (short) 92);
        this.eggIDs.put(EntityType.RABBIT, (short) 101);
        this.eggIDs.put(EntityType.ZOMBIE, (short) 54);
        this.eggIDs.put(EntityType.PIG_ZOMBIE, (short) 57);
        this.eggIDs.put(EntityType.WOLF, (short) 95);
        this.eggIDs.put(EntityType.OCELOT, (short) 98);
        this.eggIDs.put(EntityType.SHEEP, (short) 91);
        this.eggIDs.put(EntityType.PIG, (short) 90);
        this.eggIDs.put(EntityType.SPIDER, (short) 52);
        this.eggIDs.put(EntityType.BLAZE, (short) 61);
        this.eggIDs.put(EntityType.SQUID, (short) 94);
        this.eggIDs.put(EntityType.CHICKEN, (short) 93);
        this.eggIDs.put(EntityType.SLIME, (short) 55);
        this.eggIDs.put(EntityType.CAVE_SPIDER, (short) 59);
        this.eggIDs.put(EntityType.HORSE, (short) 100);
        this.eggIDs.put(EntityType.GUARDIAN, (short) 68);
        this.eggIDs.put(EntityType.MUSHROOM_COW, (short) 96);
        this.eggIDs.put(EntityType.SKELETON, (short) 51);
        this.eggIDs.put(EntityType.ENDERMAN, (short) 58);
        this.eggIDs.put(EntityType.GHAST, (short) 56);
        this.eggIDs.put(EntityType.WITCH, (short) 66);
        reload();
    }

    public double getDropPercent(EntityType entity) {
        return (this.eggDropPercents.get(entity) == null) ? 0 : this.eggDropPercents.get(entity);
    }

    public short getEggID(EntityType entity) {
        return this.eggIDs.get(entity);
    }

    public double getLootingChance() {
        return this.lootingChance;
    }

    public void reload() {
        JsonConfigurationSection mobs = this.config.getSection("mobs");

        this.eggIDs = new HashMap<EntityType, Short>();
        this.lootingChance = this.config.getDouble("lootingChance");
        for (EntityType entityType : EntityType.values()) {
            Double percent = mobs.getDouble(entityType.toString());

            if (percent != null) {
                this.eggDropPercents.put(entityType, percent);
            }
        }
    }
}
