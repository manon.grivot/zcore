package me.zodiakk.zcore.eggdrops;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;

import me.zodiakk.zcore.ZCore;
import me.zodiakk.zcore.modules.AbstractModule;
import me.zodiakk.zcore.modules.ModuleStatus;

public class EggdropsModule extends AbstractModule {

    private EggdropsListener eggdropsListener;

    public EggdropsModule() {
        super("Eggdrops");
    }

    @Override
    public ModuleStatus enable(ZCore core) {
        this.eggdropsListener = new EggdropsListener(new EggdropsConfiguration(getConfig()));
        Bukkit.getServer().getPluginManager().registerEvents(this.eggdropsListener, core);
        return setStatus(ModuleStatus.ENABLED);
    }

    @Override
    public ModuleStatus disable(ZCore core) {
        HandlerList.unregisterAll(this.eggdropsListener);
        return setStatus(ModuleStatus.DISABLED);
    }

    @Override
    public ModuleStatus reload(ZCore core) {
        return (disable(core) != ModuleStatus.DISABLED) ? getStatus() : enable(core);
    }
}
